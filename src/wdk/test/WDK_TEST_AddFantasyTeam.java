/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.test;

import wdk.data.Draft;
import wdk.gui.MessageDialog;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import wdk.data.Team;
import wdk.gui.FantasyTeamDialog;

/**
 *
 * @author Zeqli
 */
public class WDK_TEST_AddFantasyTeam extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        Draft draft = new Draft();
        MessageDialog messageDialog = new MessageDialog(primaryStage, "CLOSE");
        FantasyTeamDialog testDialog = new FantasyTeamDialog(primaryStage, draft, messageDialog);
//        testDialog.showAddFantasyTeamDialog();
        Team team = new Team("New Team", "Admin");
        team.setName("Bob's squad");
        team.setOwner("Bob Vila");
        testDialog.showEditFantasyTeamDialog(team);
        
//        testDialog.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }    
}

