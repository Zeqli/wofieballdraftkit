/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.test;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import wdk.data.Draft;
import wdk.data.Nations;
import wdk.data.Player;
import wdk.data.Team;
import wdk.gui.EditPlayerDialog;
import wdk.gui.FantasyTeamDialog;
import wdk.gui.MessageDialog;
import wdk.gui.WDK_Screen;

/**
 *
 * @author Zeqli
 */
public class WDK_TEST_EditPlayer extends Application{
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        Draft draft = new Draft();
        
        Team team = new Team("New Team", "Admin");
        team.setName("Bob's squad");
        team.setOwner("Bob Vila");
        Player player = new Player();
        player.setFirstName("Matt");
        player.setLastName("Adams");
        player.setNationOfBirth(Nations.Puerto_Rico.toString());
        player.setPosition("C");
        player.setPositions("C_U");
        player.loadProfileImage();
        player.loadNationFlagImage();
        
        MessageDialog messageDialog = new MessageDialog(primaryStage, "CLOSE");
        EditPlayerDialog testDialog = new EditPlayerDialog(primaryStage, draft, messageDialog);
//        testDialog.showAddFantasyTeamDialog();
        
        testDialog.showEditPlayerDialog(draft, player, WDK_Screen.PLAYERS)
                ;
        
//        testDialog.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }        
}
