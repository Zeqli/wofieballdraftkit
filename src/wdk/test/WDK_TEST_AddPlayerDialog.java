/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.test;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import wdk.data.Draft;
import wdk.data.Team;
import wdk.gui.AddPlayerDialog;
import wdk.gui.FantasyTeamDialog;
import wdk.gui.MessageDialog;

/**
 *
 * @author Zeqli
 */
public class WDK_TEST_AddPlayerDialog extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        Draft draft = new Draft();
        MessageDialog messageDialog = new MessageDialog(primaryStage, "CLOSE");
        AddPlayerDialog testDialog = new AddPlayerDialog(primaryStage, draft, messageDialog);
        testDialog.showAddPlayerDialog();
        
//        testDialog.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }        
}
