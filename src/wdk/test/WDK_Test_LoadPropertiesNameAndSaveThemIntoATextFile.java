/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.test;

import wdk.error.ErrorHandler;
import properties_manager.PropertiesManager;
import static wdk.WDK_StartupConstants.PATH_DATA;
import static wdk.WDK_StartupConstants.PROPERTIES_FILE_NAME;
import static wdk.WDK_StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import xml_utilities.InvalidXMLFileFormatException;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import xml_utilities.XMLUtilities;

/**
 *
 * @author Zeqli
 */
public class WDK_Test_LoadPropertiesNameAndSaveThemIntoATextFile {
    // THESE CONSTANTS ARE USED FOR LOADING PROPERTIES AS THEY ARE
    // THE ESSENTIAL ELEMENTS AND ATTRIBUTES
    public static final String PROPERTY_ELEMENT                 = "property";
    public static final String PROPERTY_LIST_ELEMENT            = "property_list";
    public static final String PROPERTY_OPTIONS_LIST_ELEMENT    = "property_options_list";
    public static final String PROPERTY_OPTIONS_ELEMENT         = "property_options";    
    public static final String OPTION_ELEMENT                   = "option";
    public static final String NAME_ATT                         = "name";
    public static final String VALUE_ATT                        = "value";
    public static final String DATA_PATH_PROPERTY               = "DATA_PATH";
    private static ArrayList<String> attNames;
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public static boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            ErrorHandler eH = ErrorHandler.getErrorHandler();
            eH.handlePropertiesFileError();
            return false;
        }        
    }
    
    
    /**
     * This function loads the xmlDataFile in this property manager, first
     * make sure it's a well formed document according to the rules specified
     * in the xmlSchemaFile.
     * 
     * @param xmlDataFile XML document to load.
     * 
     * @param xmlSchemaFile Schema that the XML document should conform to.
     * 
     * @throws InvalidXMLFileFormatException This is thrown if the XML file
     * is invalid.
     */
    public static ArrayList<String> loadProperties(String xmlDataFile, String xmlSchemaFile)
            throws InvalidXMLFileFormatException
    {
            // THIS WILL LOAD THE XML FOR US
        XMLUtilities xmlUtil = new XMLUtilities();
        
        ArrayList<String> propertiesName = new ArrayList<>();
        
        String dataPath = PATH_DATA;
 
        // ADD THE DATA PATH
        xmlDataFile = dataPath + xmlDataFile;
        xmlSchemaFile = dataPath + xmlSchemaFile;
        
        // FIRST LOAD THE FILE
        Document doc = xmlUtil.loadXMLDocument(xmlDataFile, xmlSchemaFile);
        
        // NOW LOAD ALL THE PROPERTIES
        Node propertyListNode = xmlUtil.getNodeWithName(doc, PROPERTY_LIST_ELEMENT);
        ArrayList<Node> propNodes = xmlUtil.getChildNodesWithName(propertyListNode, PROPERTY_ELEMENT);
        for(Node n : propNodes)
        {
            NamedNodeMap attributes = n.getAttributes();
            
                Node att = attributes.getNamedItem(NAME_ATT);
                String attName = attributes.getNamedItem(NAME_ATT).getTextContent();
//                String attValue = attributes.getNamedItem(VALUE_ATT).getTextContent();
                propertiesName.add(attName);
                System.out.println(attName);
            
            
        }
        
        return propertiesName;
    }
    
    
    /**
     *
     * @param argvs
     * @throws FileNotFoundException
     */
    public static void main(String argvs[]) throws FileNotFoundException, InvalidXMLFileFormatException{
        if(loadProperties()){
            System.out.println("Success Load XML File");
            attNames = loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            System.out.println(attNames.toString());
            try {
            PrintWriter writer = new PrintWriter("Properties.txt", "UTF-8");
            for(String line:attNames){
                String s = line + ',';
                writer.println(s);
            }
            writer.close();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(WDK_Test_LoadPropertiesNameAndSaveThemIntoATextFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        
        
    }
}
