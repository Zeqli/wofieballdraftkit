/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import static wdk.WDK_StartupConstants.PATH_DRAFTS;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.FreePool;
import wdk.data.Player;
import wdk.data.Team;

/**
 *
 * @author Zeqli
 */
public class JsonDraftFileManager implements DraftFileManager {
// JSON FILE READING AND WRITING CONSTANTS

    String FILE_HITTERS = "Hitters.json";
    String FILE_PITCHERS = "Pitchers.json";

    String JSON_SUBJECTS = "subjects";
    String JSON_EXT = ".json";
    String SLASH = "/";
    String ZERO = "0.000";
    String DASH = "-";

    String ROLE_HITTER = "HITTER";
    String ROLE_PITCHER = "PITCHER";

    String POSITION_PITCHER = "P";

    // PLAYER INFO 
    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers";
    String JSON_TEAM = "TEAM";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_QP = "QP";
    String JSON_AB = "AB";
    String JSON_R = "R";
    String JSON_H = "H";
    String JSON_HR = "HR";
    String JSON_RBI = "RBI";
    String JSON_STEAL_BASE = "SB";
    String JSON_NOTES = "NOTES";
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";

    String JSON_IP = "IP";
    String JSON_ER = "ER";
    String JSON_W = "W";
    String JSON_SV = "SV";
    String JSON_BB = "BB";
    String JSON_K = "K";

    String JSON_NAME = "Name";
    String JSON_OWNER = "Owner";
    String JSON_STARTUPLINE = "StartUpLine";
    String JSON_TAXI_SQUAD = "TaxiSqua";
    String JSON_AVAILABLE_POS = "Available_Positions";
    String JSON_TOTLE_PLAYER = "Total_Players";
    String JSON_PURSE = "Purse";
    String JSON_RUNS = "Runs";
    String JSON_RUNS_BATTLE_IN = "Runs_Battle_In";
    String JSON_STOLEN_BASES = "Stolen_Base";
    String JSON_BATING_AVG = "Bating_Average";
    String JSON_WINS = "Wins";
    String JSON_STRIKE_OUTS = "Strike_Outs";
    String JSON_SAVES = "Saves";
    String JSON_EARN_RUN_AVG = "Earn_Run_Average";
    String JSON_WHIP = "Whip";
    String JSON_TOTALPOINTS = "Total_Points";
    String JSON_DRAFT_NAME = "Draft_Name";
    String JSON_CURRENT_TEAM_NAME = "Current_Team_Name";
    String JSON_FREE_POOL = "Free_Pool";
    String JSON_ALL_TEAM = "Teams";
    String JSON_PRO_TEAM = "Pro_Team";
    String JSON_POSITION = "Position";
    String JSON_POSITIONS = "Positions";
    String JSON_RORW = "RorW";
    String JSON_HRORSV = "HrOrSv";
    String JSON_RBIORK = "RBIorK";
    String JSON_SBORERA = "SbOrEra";
    String JSON_BARORWHIP = "BarOrWhip";
    String JSON_CONTRACT = "Contract";
    String JSON_SALARY = "Salary";
    String JSON_NATIONS_OF_BIRTH = "NationsOfBirth";
    String JSON_ROLE = "Role";
    String JSON_CURRENT_TEAM = "CurrentTeam";
    String JSON_PP = "pp";
    String JSON_HOME_RUNS = "Home_Runs";
    String JSON_TAXI_CAPACITY = "taxiCapacity";

    /**
     * This method saves all the data associated with a draft to a JSON file.
     * 保存关于draft的所有data到JSON file
     *
     * @param draftToSave The draft whose data we are saving.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    @Override
    public void saveDraft(Draft draftToSave) throws IOException {
        // BUILD THE FILE PATH
        String draftListing = "" + draftToSave.getDraftName();
        String jsonFilePath = PATH_DRAFTS + draftListing + JSON_EXT;

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);

        // MAKE A OBJECT TO STORE DRAFT NAME
        // AND CURRENT TEAM
        // THE FREE POOL
        JsonObject freePoolJsonObject = makeFreePoolJsonObject((FreePool) draftToSave.getFreeAgent());

        // FINALLY, ALL TAEMS IN THE DRAFT
        JsonArray teamsJsonArray = makeAllTeamJsonArray(draftToSave.getAllTeams());

        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject draftJsonObject = Json.createObjectBuilder()
                .add(JSON_DRAFT_NAME, draftToSave.getDraftName())
                .add(JSON_CURRENT_TEAM_NAME, draftToSave.getCurrentTeam().getName())
                .add(JSON_FREE_POOL, freePoolJsonObject)
                .add(JSON_ALL_TEAM, teamsJsonArray)
                .build();

        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(draftJsonObject);
    }

    /**
     * Loads the draftToLoad argument using the data found in the json file.
     *
     * @param draftToLoad Draft to load.
     * @param jsonFilePath File containing the data to load.
     *
     * @throws IOException Thrown when IO fails.
     */
    @Override
    public void loadDraft(Draft draftToLoad, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);

        // NOW LOAD THE DRAFT NAME
        draftToLoad.setDraftName(json.getString(JSON_DRAFT_NAME));

        // NOW LOAD THE TEAMS
        JsonArray teams = json.getJsonArray(JSON_ALL_TEAM);
        draftToLoad.clearTeams();
        for (int i = 0; i < teams.size(); i++) {
            // LOAD IT TO TEAM OBJECT
            Team team = loadTeamJsonObject(teams.getJsonObject(i));
            // ADD IT TO THE ALL TEAM LIST
            draftToLoad.addTeam(team);
        }

        // NOW LOAD THE FREE POOL
        JsonObject freeAgent = json.getJsonObject(JSON_FREE_POOL);
        JsonArray freeAgentPlayers = freeAgent.getJsonArray(JSON_STARTUPLINE);

        draftToLoad.clearFreeAgentPlayers();
        for (int i = 0; i < freeAgentPlayers.size(); i++) {
            // LOAD IT TO PLAYER OBJECT
            Player player = loadPlayerFromJsonObject(freeAgentPlayers.getJsonObject(i));

            // ADD IT TO THE FREE AGENT
            draftToLoad.addPlayerToFreeAgent(player);
        }

        // NOW THE CURRENT TEAM
        draftToLoad.setCurrentTeam(draftToLoad.getTeam(json.getString(JSON_CURRENT_TEAM_NAME)));

    }

    @Override
    public void loadAllPlayers(DraftDataManager data, String filePath) throws IOException {

        // LOAD THE HITTER DATA FROM JSON FILE
        loadHitters(data, filePath + FILE_HITTERS);

        // LOAD THE PITCHER DATA FROM JSON FILE
        loadPitchers(data, filePath + FILE_PITCHERS);

    }

    private void loadHitters(DraftDataManager data, String filePath) throws IOException {

//            "TEAM":"COL",
//            "LAST_NAME":"Adames",
//            "FIRST_NAME":"Cristhian",
//            "QP":"SS",
//            "AB":"15",
//            "R":"1",
//            "H":"1",
//            "HR":"0",
//            "RBI":"0",
//            "SB":"0",
//            "NOTES":"2_SS",
//            "YEAR_OF_BIRTH":"1991",
//            "NATION_OF_BIRTH":"Dominican_Republic"
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        JsonArray jsonHitterArray = json.getJsonArray(JSON_HITTERS);
        for (int i = 0; i < jsonHitterArray.size(); i++) {
            JsonObject jso = jsonHitterArray.getJsonObject(i);
            String team = jso.getString(JSON_TEAM);
            String lastName = jso.getString(JSON_LAST_NAME);
            String firstName = jso.getString(JSON_FIRST_NAME);
            String qualifiedPos = jso.getString(JSON_QP);
            int atBats = Integer.valueOf(jso.getString(JSON_AB));
            int runs = Integer.valueOf(jso.getString(JSON_R));
            int hits = Integer.valueOf(jso.getString(JSON_H));
            int homeRuns = Integer.valueOf(jso.getString(JSON_HR));
            int runsBattleIn = Integer.valueOf(jso.getString(JSON_RBI));
            String stealBase = jso.getString(JSON_STEAL_BASE);
            String notes = jso.getString(JSON_NOTES);
            String yearOfBirth = jso.getString(JSON_YEAR_OF_BIRTH);
            String nationOfBirth = jso.getString(JSON_NATION_OF_BIRTH);

            DecimalFormat myFormatter = new DecimalFormat("0.000");
            String batAvg = atBats == 0 ? ZERO : myFormatter.format(hits / atBats);

            Player player = new Player();
            player.setRole(ROLE_HITTER);
            player.setFirstName(firstName);
            player.setLastName(lastName);
            player.setPreviousTeam(team);
            player.setPositions(qualifiedPos);
            player.setYearOfBirth(yearOfBirth);
            player.setROrW(runs);
            player.setHrOrSv(homeRuns);
            player.setRbiOrK(runsBattleIn);
            player.setSbOrEra(stealBase);
            player.setBaOrWhip(batAvg);
            player.setNotes(notes);
            player.setNationOfBirth(nationOfBirth);

            // GET PLAYER IMAGE
            player.loadProfileImage();
            player.loadNationFlagImage();

            data.addPlayer(player);
        }
    }

    private void loadPitchers(DraftDataManager data, String filePath) throws IOException {

//            "TEAM":"LAD",
//            "LAST_NAME":"Anderson",
//            "FIRST_NAME":"Chris",
//            "IP":"0",
//            "ER":"0",
//            "W":"0",
//            "SV":"0",
//            "H":"0",
//            "BB":"0",
//            "K":"0",
//            "NOTES":"S8",
//            "YEAR_OF_BIRTH":"1992",
//            "NATION_OF_BIRTH":"USA"        
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        JsonArray jsonPitcherArray = json.getJsonArray(JSON_PITCHERS);
        for (int i = 0; i < jsonPitcherArray.size(); i++) {
            JsonObject jso = jsonPitcherArray.getJsonObject(i);
            String team = jso.getString(JSON_TEAM);
            String lastName = jso.getString(JSON_LAST_NAME);
            String firstName = jso.getString(JSON_FIRST_NAME);
            double inningsPitched = Double.valueOf(jso.getString(JSON_IP));
            int earnedRuns = Integer.valueOf(jso.getString(JSON_ER));
            int wins = Integer.valueOf(jso.getString(JSON_W));
            int saves = Integer.valueOf(jso.getString(JSON_SV));
            int hits = Integer.valueOf(jso.getString(JSON_H));
            int walks = Integer.valueOf(jso.getString(JSON_BB));
            int strikeOuts = Integer.valueOf(jso.getString(JSON_K));
            String notes = jso.getString(JSON_NOTES);
            String yearOfBirth = jso.getString(JSON_YEAR_OF_BIRTH);
            String nationOfBirth = jso.getString(JSON_NATION_OF_BIRTH);

            DecimalFormat myFormatter = new DecimalFormat("0.00");
            String whip = inningsPitched == 0 ? "0.00" : myFormatter.format((walks + hits) / inningsPitched);
            String era = inningsPitched == 0 ? "0.00" : myFormatter.format((9 * earnedRuns) / inningsPitched);

            Player player = new Player();
            player.setRole(ROLE_PITCHER);
            player.setFirstName(firstName);
            player.setLastName(lastName);
            player.setPreviousTeam(team);
            player.setPositions(POSITION_PITCHER);
            player.setYearOfBirth(yearOfBirth);
            player.setROrW(wins);
            player.setHrOrSv(saves);
            player.setRbiOrK(strikeOuts);
            player.setSbOrEra(era);
            player.setBaOrWhip(whip);
            player.setNotes(notes);
            player.setNationOfBirth(nationOfBirth);

            // GET PLAYER IMAGE
            player.loadProfileImage();
            player.loadNationFlagImage();

            data.addPlayer(player);
        }
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }

    // LOAD TEAM OBJECT
    private Team loadTeamJsonObject(JsonObject jso) {
        Team team = new Team(jso.getString(JSON_NAME), jso.getString(JSON_OWNER));

        // STARTUP LINE
        JsonArray startUpLineArray = jso.getJsonArray(JSON_STARTUPLINE);
        HashMap<String, Player> startHash = new HashMap<>();
        for (int i = 0; i < startUpLineArray.size(); i++) {
            Player player = loadPlayerFromJsonObject(startUpLineArray.getJsonObject(i));
            String keyName = player.getFirstName() + player.getLastName();
            startHash.put(keyName, player);
        }
        team.setStartUpLine(startHash);

        // TAXI SQUAD
        JsonArray taxiArray = jso.getJsonArray(JSON_TAXI_SQUAD);
        HashMap<String, Player> taxiHash = new HashMap<>();
        for (int i = 0; i < taxiArray.size(); i++) {
            Player player = loadPlayerFromJsonObject(taxiArray.getJsonObject(i));
            String keyName = player.getFirstName() + player.getLastName();
            taxiHash.put(keyName, player);
        }
        team.setTaxiSquad(taxiHash);

        JsonArray availablePosArray = jso.getJsonArray(JSON_AVAILABLE_POS);
        HashMap<String, Integer> availablePosHash = new HashMap<>();
        for (int i = 0; i < availablePosArray.size(); i++) {
            String availablePosKeyValueString = availablePosArray.getString(i);
            String[] entry = availablePosKeyValueString.split("\\|");
            availablePosHash.put(entry[0], Integer.valueOf(entry[1]));
        }
        team.setAvailablePosition(availablePosHash);
        team.setTotalPlayers(jso.getInt(JSON_TOTLE_PLAYER));
        team.setPurse(jso.getInt(JSON_PURSE));
        team.setPp(jso.getInt(JSON_PP));
        team.setRuns(jso.getInt(JSON_RUNS));
        team.setHomeRuns(jso.getInt(JSON_HOME_RUNS));
        team.setRunsBattleIn(jso.getInt(JSON_RUNS_BATTLE_IN));
        team.setStolenBases(jso.getInt(JSON_STOLEN_BASES));
        team.setBattingAverage(jso.getString(JSON_BATING_AVG));
        team.setWins(jso.getInt(JSON_WINS));
        team.setStrikeOuts(jso.getInt(JSON_STRIKE_OUTS));
        team.setSaves(jso.getInt(JSON_SAVES));
        team.setEarnRunAverage(jso.getString(JSON_EARN_RUN_AVG));
        team.setWhip(jso.getString(JSON_WHIP));
        team.setTotalPoints(jso.getInt(JSON_TOTALPOINTS));
        team.setTaxiCapacity(jso.getInt(JSON_TAXI_CAPACITY));
        team.refreshTeamData();
        return team;
    }

    private Player loadPlayerFromJsonObject(JsonObject jso) {
        Player player = new Player();

        player.setFirstName(jso.getString(JSON_FIRST_NAME));
        player.setLastName(jso.getString(JSON_LAST_NAME));
        player.setPreviousTeam(jso.getString(JSON_PRO_TEAM));
        player.setPosition(jso.getString(JSON_POSITION));
        player.setPositions(jso.getString(JSON_POSITIONS));
        player.setYearOfBirth(jso.getString(JSON_YEAR_OF_BIRTH));
        player.setROrW(jso.getInt(JSON_RORW));
        player.setHrOrSv(jso.getInt(JSON_HRORSV));
        player.setRbiOrK(jso.getInt(JSON_RBIORK));
        player.setSbOrEra(jso.getString(JSON_SBORERA));
        player.setBaOrWhip(jso.getString(JSON_BARORWHIP));
        player.setContract(jso.getString(JSON_CONTRACT));
        player.setSalary(jso.getInt(JSON_SALARY));
        player.setNotes(jso.getString(JSON_NOTES));
        player.setNationOfBirth(jso.getString(JSON_NATIONS_OF_BIRTH));
        player.setRole(jso.getString(JSON_ROLE));
        player.setCurrentTeam(jso.getString(JSON_CURRENT_TEAM));

        // GET PLAYER IMAGE
        player.loadProfileImage();
        player.loadNationFlagImage();
        return player;
    }

    // BUILDS AND RETURNS A JsonArray CONTAINING THE PROVIDED DATA
    public JsonArray buildJsonArray(List<Object> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Object d : data) {
            jsb.add(d.toString());
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    // BUILDS AND RETURNS A JsonObject CONTAINING A JsonArray
    // THAT CONTAINS THE PROVIDED DATA
    public JsonObject buildJsonArrayObject(List<Object> data) {
        JsonArray jA = buildJsonArray(data);
        JsonObject arrayObject = Json.createObjectBuilder().add(JSON_SUBJECTS, jA).build();
        return arrayObject;
    }

    // MAKE A DRAFT NAME OBJECT
    // MAKE A CURRENT TEAM NAME OBJECT
    // MAKE A FREE AGENT OBJECT (SAME US TEAMS OBJECT)
    // MAKE AN ARRAY OF TEAMS LIST
    private JsonArray makeAllTeamJsonArray(ObservableList<Team> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Team t : data) {
            jsb.add(makeTeamJsonObject(t));
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    // MAKES AND RETURNS A JSON OBJECT FOR TEAMS LIST
    private JsonObject makeTeamJsonObject(Team teamToSave) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_NAME, teamToSave.getName())
                .add(JSON_OWNER, teamToSave.getOwner())
                .add(JSON_STARTUPLINE, makeStartUpLineJsonArray(teamToSave.getStartUpLine()))
                .add(JSON_TAXI_SQUAD, makeTaxiSquadJsonArray(teamToSave.getTaxiSquad()))
                .add(JSON_AVAILABLE_POS, makeAvailablePositionJsonObject(teamToSave.getAvailablePosition()))
                .add(JSON_TOTLE_PLAYER, teamToSave.getTotalPlayers())
                .add(JSON_PP, teamToSave.getPp())
                .add(JSON_PURSE, teamToSave.getPurse())
                .add(JSON_RUNS, teamToSave.getRuns())
                .add(JSON_HOME_RUNS, teamToSave.getHomeRuns())
                .add(JSON_RUNS_BATTLE_IN, teamToSave.getRunsBattleIn())
                .add(JSON_STOLEN_BASES, teamToSave.getStolenBases())
                .add(JSON_BATING_AVG, teamToSave.getBattingAverage())
                .add(JSON_WINS, teamToSave.getWins())
                .add(JSON_STRIKE_OUTS, teamToSave.getStrikeOuts())
                .add(JSON_SAVES, teamToSave.getSaves())
                .add(JSON_EARN_RUN_AVG, teamToSave.getEarnRunAverage())
                .add(JSON_WHIP, teamToSave.getWhip())
                .add(JSON_TOTALPOINTS, teamToSave.getTotalPoints())
                .add(JSON_TAXI_CAPACITY, teamToSave.getTaxiCapacity())
                .build();
        return jso;
    }

    // MAKES AND RETURNS A JSON OBJECT FOR THE STARTUP LINE ARRAY
    private JsonArray makeStartUpLineJsonArray(HashMap<String, Player> startUpLine) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Map.Entry<String, Player> entry : startUpLine.entrySet()) {
            jsb.add(makePlayerJsonObject(entry.getValue()));
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    // MAKES AND RETURNS A JSON OBJECT FOR THE TAXI SQUAD ARRAY
    private JsonArray makeTaxiSquadJsonArray(HashMap<String, Player> taxiSquad) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Map.Entry<String, Player> entry : taxiSquad.entrySet()) {
            jsb.add(makePlayerJsonObject(entry.getValue()));
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    // MAKES AND RETURNS A JSON OBJECT FOR THE PLAYER
    private JsonObject makePlayerJsonObject(Player player) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_FIRST_NAME, player.getFirstName())
                .add(JSON_LAST_NAME, player.getLastName())
                .add(JSON_PRO_TEAM, player.getPreviousTeam())
                .add(JSON_POSITION, player.getPosition())
                .add(JSON_POSITIONS, player.getPositions())
                .add(JSON_YEAR_OF_BIRTH, player.getYearOfBirth())
                .add(JSON_RORW, player.getROrW())
                .add(JSON_HRORSV, player.getHrOrSv())
                .add(JSON_RBIORK, player.getRbiOrK())
                .add(JSON_SBORERA, player.getSbOrEra())
                .add(JSON_BARORWHIP, player.getBaOrWhip())
                .add(JSON_CONTRACT, player.getContract())
                .add(JSON_SALARY, player.getSalary())
                .add(JSON_NOTES, player.getNotes())
                .add(JSON_NATIONS_OF_BIRTH, player.getNationOfBirth())
                .add(JSON_ROLE, player.getRole())
                .add(JSON_CURRENT_TEAM, player.getCurrentTeam()).build();

        return jso;
    }

    private JsonArray makeAvailablePositionJsonObject(HashMap<String, Integer> availablePositions) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Map.Entry<String, Integer> entry : availablePositions.entrySet()) {
            jsb.add(entry.getKey() + "|" + entry.getValue());
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    // MAKES AND RETURNS A JSON OBJECT FOR THE FREE POOL
    private JsonObject makeFreePoolJsonObject(FreePool teamToSave) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_STARTUPLINE, makeStartUpLineJsonArray(teamToSave.getStartUpLine()))
                .build();
        return jso;
    }

}
