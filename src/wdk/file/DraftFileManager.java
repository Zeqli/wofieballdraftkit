/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.IOException;
import wdk.data.Draft;
import wdk.data.DraftDataManager;

/**
 *
 * @author Zeqli
 */
public interface DraftFileManager {

    public void saveDraft(Draft courseToSave) throws IOException;
    public void loadDraft(Draft courseToLoad, String coursePath) throws IOException;
    public void loadAllPlayers(DraftDataManager data, String filePath) throws IOException;

}
