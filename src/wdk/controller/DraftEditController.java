/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import static wdk.controller.FantasyTeamEditController.TAXI_SQUAD;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.Player;
import wdk.data.Team;
import wdk.error.ErrorHandler;
import wdk.gui.WDK_GUI;
import wdk.gui.WDK_Screen;

/**
 * This controller class handles the responses to all draft editing input,
 * including verification of data and binding of entered data to the Draft
 * object.
 *
 * @author Zeqing Li
 */
public class DraftEditController {

    // WE USE THIS TO MAKE SURE OUR PROGRAMMED UPDATES OF UI
    // VALUES DON'T USE THEMSELVES TRIGGER EVENTS
    private boolean enabled;

    /**
     * Constructor that gets this controller ready, not much to initialize as
     * the methods for this function are sent all the objects they need as
     * arguments.
     */
    public DraftEditController() {
        enabled = true;
    }

    /**
     * This mutator method lets us enable or disable this controller.
     *
     * @param enableSetting If false, this controller will not respond to Draft
     * editing. If true, it will.
     */
    public void enable(boolean enableSetting) {
        enabled = enableSetting;
    }

    /**
     * This controller function is called in response to the user changing draft
     * details in the UI. It responds by updating the bound Draft object using
     * all the UI values, including the verification of that data.
     *
     * @param gui The user interface that requested the change.
     */
    public void handleDraftChangeRequest(WDK_GUI gui) {
        if (enabled) {
            try {
                // UPDATE THE COURSE, VERIFYING INPUT VALUES
                gui.updateDraftInfo(gui.getDataManager().getDraft());

                // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
                // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
                // THE SAVE BUTTON IS ENABLED
                gui.getFileController().markAsEdited(gui);
            } catch (Exception e) {
                // SOMETHING WENT WRONG
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                eH.handleUpdateDraftError();
            }
        }
    }

    public void handleSwitchSelectedFantasyTeamRequest(WDK_GUI gui, String currentTeamName) {
        DraftDataManager dm = gui.getDataManager();
        Draft draft = dm.getDraft();
        Team currentTeam = draft.getTeam(currentTeamName);
        if (enabled) {
            try {
                gui.getDataManager().getDraft().setCurrentTeam(currentTeam);

                // IT WILL UPDATE TEH TABLE 
                // UPDATE THE COURSE, VERIFYING INPUT VALUES
                gui.updateDraftInfo(gui.getDataManager().getDraft());

                // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
                // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
                // THE SAVE BUTTON IS ENABLED
                gui.getFileController().markAsEdited(gui);
            } catch (Exception e) {
                // SOMETHING WENT WRONG
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                eH.handleUpdateDraftError();
            }
        }
    }

    public Player handleSelectPlayerRequest(WDK_GUI gui) {
        DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();

        // GET ALL PLAYRES 
        ObservableList<Player> freePlayers = draft.getFreeAgent().getPlayersData();
        Player qualifiedPlayer = selectQualifiedPlayer(draft, freePlayers);
        if(!qualifiedPlayer.equals(new Player())){
        // SET CONTRACT TO S2 AND COST TO $1

            // REMOVE IT FROM OLD TEAM
            draft.getFreeAgent().removePlayer(qualifiedPlayer);
            qualifiedPlayer.setCurrentTeam(draft.getCurrentTeam().getName());
            

            if (qualifiedPlayer.getPosition().equals(TAXI_SQUAD)) {
                qualifiedPlayer.setPosition(qualifiedPlayer.getRole());
                qualifiedPlayer.setContract("X");
                qualifiedPlayer.setSalary(1);
            } else {
                qualifiedPlayer.setContract("S2");
                qualifiedPlayer.setSalary(1);
            }
            // UPDATE FANTASY TEAM
            draft.updateDraftSummaryList(qualifiedPlayer);
            draft.getCurrentTeam().addPlayer(qualifiedPlayer,qualifiedPlayer.getPosition());

            // IT WILL UPDATE TEH TABLE 
            // UPDATE THE COURSE, VERIFYING INPUT VALUES
            gui.updateDraftInfo(gui.getDataManager().getDraft());
        } else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
        return qualifiedPlayer;

    }
    
    // CHECK IS A PLAYER QUALIFY A TEAM
    private Player selectQualifiedPlayer(Draft draft, ObservableList<Player> freePlayers){
        for (Player playerToSelect : freePlayers) {
            ArrayList<String> positions = new ArrayList<>();
            draft.getCurrentTeam().getAvailablePositionList().stream().forEach((pos) -> {
                positions.add(pos);
            });
            // FIND A QUALIFIED PLAYER
            for (String pos : positions) {
                if (playerToSelect.getPositions().contains(pos)) {
                    // SELECT THIS PLAYER
                    playerToSelect.setPosition(pos);
                    return playerToSelect;
                }
            }
            // IF TAXI SQUAD AVAILABLE AND
            if (draft.getCurrentTeam().isStartUpLineFull() && !draft.getCurrentTeam().isFull()) {
                playerToSelect.setPosition(TAXI_SQUAD);
                return playerToSelect;
            }
        }
        return new Player();
    }

    public void handleAutoSelectPlayerRequest(WDK_GUI gui) {
        ReentrantLock progressLock = new ReentrantLock();
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    progressLock.lock();
                    while(!handleSelectPlayerRequest(gui).equals(new Player()) && !gui.getDataManager().getDraft().getCurrentTeam().isFull()) {
                        handleSelectPlayerRequest(gui);
                        
                        // SLEEP EACH FRAME
//                try {
                        Thread.sleep(1000);
//                } catch (InterruptedException ie) {
//                    ie.printStackTrace();
//                }
                    }
                } finally {
                    progressLock.unlock();
                }
                return null;
            }
        };
         // THIS GETS THE THREAD ROLLING
        Thread thread = new Thread(task);
        thread.start();
    }

}
