/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.controller;

import java.util.ArrayList;
import javafx.stage.Stage;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.Player;
import wdk.data.Team;
import wdk.gui.AddPlayerDialog;
import wdk.gui.EditPlayerDialog;
import wdk.gui.FantasyTeamDialog;
import wdk.gui.MessageDialog;
import wdk.gui.WDK_GUI;
import wdk.gui.WDK_Screen;
import wdk.gui.YesNoCancelDialog;

/**
 *
 * @author Zeqli
 */
public class FantasyTeamEditController {

    AddPlayerDialog ad;
    EditPlayerDialog ed;
    FantasyTeamDialog fd;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;

    // CONFIRMATION INFO
    public static final String REMOVE_TEAM_CONFIRMATION = "Want to remove team?";
    public static final String REMOVE_PLAYER_CONFIRMATION = "Want to remove this player?";
    
    static final String TAXI_SQUAD = "Taxi Squad";

    public FantasyTeamEditController(Stage initPrimaryStage, Draft draft, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        ad = new AddPlayerDialog(initPrimaryStage, draft, initMessageDialog);
        ed = new EditPlayerDialog(initPrimaryStage, draft, messageDialog);
        fd = new FantasyTeamDialog(initPrimaryStage, draft, messageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    public void handleEditPlayerRequest(WDK_GUI gui, Player playerToEdit,  WDK_Screen screen) {
        DraftDataManager cdm = gui.getDataManager();
        Draft draft = cdm.getDraft();

        ed.showEditPlayerDialog(gui.getDataManager().getDraft(), playerToEdit, screen);

        // DID THE USER CONFIRM?
        if (ed.wasCompleteSelected()) {
            // REMOVE IT FROM OLD TEAM
            Team oldTeam = draft.getTeam(playerToEdit.getCurrentTeam());
            oldTeam.removePlayer(playerToEdit);
            
            // UPDATE PLAYER
            Player p = ed.getPlayer();
            playerToEdit.setCurrentTeam(p.getCurrentTeam());
            if(p.getPosition().equals(TAXI_SQUAD)){
                playerToEdit.setPosition(p.getRole());
                playerToEdit.setContract("X");
                playerToEdit.setSalary(1);
            }
            else{
                playerToEdit.setPosition(p.getPosition());
                playerToEdit.setContract(p.getContract());
                playerToEdit.setSalary(p.getSalary());
            }
            // UPDATE FANTASY TEAM
            draft.updateDraftSummaryList(playerToEdit);
            Team newTeam = draft.getTeam(p.getCurrentTeam());
            newTeam.addPlayer(playerToEdit, p.getPosition());

            // IT WILL UPDATE TEH TABLE 
            // UPDATE THE COURSE, VERIFYING INPUT VALUES
            gui.updateDraftInfo(gui.getDataManager().getDraft());
        } else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }

    public void handleAddFantasyTeamRequest(WDK_GUI gui) {

        DraftDataManager cdm = gui.getDataManager();
        Draft draft = cdm.getDraft();
        fd.showAddFantasyTeamDialog();

        // DID THE USER CONFIRM?
        if (fd.wasCompleteSelected()) {
            // GET THE NEW TEAM
            Team t = fd.getFantasyTeam();

            // AND ADD IT AS A ROW TO THE TABLE
            draft.addTeam(t);
            gui.updateDraftInfo(gui.getDataManager().getDraft());
        } else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }

    public void handleEditFantasyTeamRequest(WDK_GUI gui, String teamName) {
        DraftDataManager cdm = gui.getDataManager();
        Draft draft = cdm.getDraft();
        Team teamToEdit = draft.getTeam(teamName);
        fd.showEditFantasyTeamDialog(teamToEdit);

        // DID THE USER CONFIRM?
        if (fd.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Team t = fd.getFantasyTeam();
            teamToEdit.setName(t.getName());
            teamToEdit.setOwner(t.getOwner());
            gui.updateDraftInfo(gui.getDataManager().getDraft());
        } else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }

    public void handleRemoveFantasyTeamRequest(WDK_GUI gui, String teamName) {
        DraftDataManager cdm = gui.getDataManager();
        Draft draft = cdm.getDraft();
        Team teamToRemove = draft.getTeam(teamName);

        yesNoCancelDialog.show(REMOVE_TEAM_CONFIRMATION);
        if (yesNoCancelDialog.getSelection().equals(yesNoCancelDialog.YES)) {
            // RELEASE PLAYERS TO FREE POOL
            ArrayList<Player> releasedPlayers = teamToRemove.releasePlayers();
            for (Player p : releasedPlayers) {
                p.setCurrentTeam(draft.getFreeAgent().getName());
            }

            // REMOVE TEAM
            draft.removeTeam(teamToRemove);
            draft.resetCurrentTeam();
            
            
            // IT WILL UPDATE TEH TABLE 
            // UPDATE THE COURSE, VERIFYING INPUT VALUES
            gui.updateDraftInfo(gui.getDataManager().getDraft());
        } else {
            // DO NOTHING
        }

    }

    public void handleAddNewPlayerRequest(WDK_GUI gui) {
        DraftDataManager cdm = gui.getDataManager();
        Draft draft = cdm.getDraft();
        ad.showAddPlayerDialog();

        // DID THE USER CONFIRM?
        if (ad.wasCompleteSelected()) {
            // GET THE NEW TEAM
            Player p = ad.getPlayer();

            // AND ADD IT AS A ROW TO THE TABLE
            draft.addPlayerToFreeAgent(p);
            gui.updateDraftInfo(gui.getDataManager().getDraft());
        } else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }

    }

    public void handleDeletePlayerRequest(WDK_GUI gui, Player playerToBeRemove) {
        DraftDataManager cdm = gui.getDataManager();
        Draft draft = cdm.getDraft();

        yesNoCancelDialog.show(REMOVE_PLAYER_CONFIRMATION);
        if (yesNoCancelDialog.getSelection().equals(yesNoCancelDialog.YES)) {
            draft.deletePlayer(playerToBeRemove);
            gui.updateDraftInfo(gui.getDataManager().getDraft());
        } else {
            // DO NOTHING
        }
    }
    
    
}
