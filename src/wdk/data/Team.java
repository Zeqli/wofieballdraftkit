/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * ToDo: int 都要改成IntegerProperty, String, String 同理
 *
 * @author Zeqli
 */
public class Team {

    StringProperty name;
    StringProperty owner;
    HashMap<String, Player> startUpLine;
    HashMap<String, Player> taxiSquad;
    HashMap<String, Integer> availablePosition;
    int totalPlayers;
    IntegerProperty playerNeeded;
    IntegerProperty purse;
    IntegerProperty pp;
    IntegerProperty runs;
    IntegerProperty homeRuns;
    IntegerProperty runsBattleIn;
    IntegerProperty stolenBases;
    StringProperty battingAverage;
    IntegerProperty wins;
    IntegerProperty saves;
    IntegerProperty strikeOuts;
    StringProperty earnRunAverage;
    StringProperty whip;
    IntegerProperty totalPoints;
    int taxiCapacity;

    final static String C_POSITION = "C";
    final static String FIRST_BASE_POSITION = "1B";
    final static String CI_POSITION = "CI";
    final static String THIRD_BAES_POSITION = "3B";
    final static String SECOND_BASE_POSITION = "2B";
    final static String MI_POSITION = "MI";
    final static String SS_POSITION = "SS";
    final static String U_POSITION = "U";
    final static String OF_POSITION = "OF";
    final static String P_POSITION = "P";

    final static String ROLE_HITTER = "HITTER";
    final static String ROLE_PITCHER = "PITCHER";
    
    public static final String FREE_AGENT = "Free Agent";

    final static int MAX_PLAYERS_IN_A_TEAM = 23;
    final static int INITIAL_MONEY = 260;
    final static String INIT_BA = "0.000";
    final static String INIT_W = "0.0";
    final static String INIT_EAR = "0.00";
    final static String INIT_WHIP = "0.00";

    final static int MINUE_ONE = -1;
    final static int ZERO = 0;
    final static int ONE = 1;
    final static int TWO = 2;
    final static int FIVE = 5;
    final static int NINE = 9;
    final static int FOUR_TEEN = 14;

    static final String POS_C = "C";
    static final String POS_1B = "1B";
    static final String POS_CI = "CI";
    static final String POS_3B = "3B";
    static final String POS_2B = "2B";
    static final String POS_MI = "MI";
    static final String POS_SS = "SS";
    static final String POS_OF = "OF";
    static final String POS_U = "U";
    static final String POS_P = "P";

    public Team(String teamName, String ownerName) {
        name = new SimpleStringProperty(teamName);
        owner = new SimpleStringProperty(ownerName);
        startUpLine = new HashMap<>();
        taxiSquad = new HashMap<>();
        availablePosition = new HashMap<>();
        totalPlayers = 0;
        taxiCapacity = 8;
        playerNeeded = initIntegerProperty(MAX_PLAYERS_IN_A_TEAM);
        purse = initIntegerProperty(INITIAL_MONEY);
        pp = initIntegerProperty(ZERO);
        runs = initIntegerProperty(ZERO);
        homeRuns = new SimpleIntegerProperty(ZERO);
        runsBattleIn = initIntegerProperty(ZERO);
        stolenBases = initIntegerProperty(ZERO);
        battingAverage = new SimpleStringProperty(INIT_BA);
        wins = initIntegerProperty(ZERO);
        strikeOuts = initIntegerProperty(ZERO);
        saves = initIntegerProperty(ZERO);;
        earnRunAverage = new SimpleStringProperty(INIT_EAR);
        whip = new SimpleStringProperty(INIT_WHIP);
        totalPoints = initIntegerProperty(ZERO);

        availablePosition.put(C_POSITION, TWO);
        availablePosition.put(FIRST_BASE_POSITION, ONE);
        availablePosition.put(CI_POSITION, ONE);
        availablePosition.put(THIRD_BAES_POSITION, ONE);
        availablePosition.put(SECOND_BASE_POSITION, ONE);
        availablePosition.put(MI_POSITION, ONE);
        availablePosition.put(SS_POSITION, ONE);
        availablePosition.put(U_POSITION, ONE);
        availablePosition.put(OF_POSITION, FIVE);
        availablePosition.put(P_POSITION, NINE);
    }

 
 
    
    /**
     ***************************** GETTERS
     *
     ******************************
     * @return
     */
    public String getName() {
        return name.get();
    }

    public String getOwner() {
        return owner.get();
    }

    public HashMap<String, Player> getStartUpLine() {
        return startUpLine;
    }

    public HashMap<String, Player> getTaxiSquad() {
        return taxiSquad;
    }

    public int getPlayerNeeded() {
        return MAX_PLAYERS_IN_A_TEAM - totalPlayers;
    }

    public int getTotalPlayers() {
        return totalPlayers;
    }

    public int getPurse() {
        return purse.get();
    }

    public int getRuns() {
        return runs.get();
    }
    
    public int getHomeRuns() {
        return homeRuns.get();
    }


    public int getRunsBattleIn() {
        return runsBattleIn.get();
    }

    public int getStolenBases() {
        return stolenBases.get();
    }

    public String getBattingAverage() {
        return battingAverage.get();
    }

    public int getWins() {
        return wins.get();
    }

    public int getStrikeOuts() {
        return strikeOuts.get();
    }

    public int getSaves() {
        return saves.get();
    }

    public String getEarnRunAverage() {
        return earnRunAverage.get();
    }

    public String getWhip() {
        return whip.get();
    }

    public int getTotalPoints() {
        return totalPoints.get();
    }

    public int getPp() {
        return pp.get();
    }
   public int getTaxiCapacity() {
        return taxiCapacity;
    }

    /**
     ***************************** GETTERS******************************
     */
    /**
     ***************************** SETTERS******************************
     */

    public void setName(String name) {
        this.name.set(name);
    }

    public void setOwner(String owner) {
        this.owner.set(owner);
    }

    public void setStartUpLine(HashMap<String, Player> startUpLine) {
        this.startUpLine = startUpLine;
    }

    public void setTaxiSquad(HashMap<String, Player> taxiSquad) {
        this.taxiSquad = taxiSquad;
    }

    public void setAvailablePosition(HashMap<String, Integer> availablePosition) {
        this.availablePosition = availablePosition;
    }

    public void setTotalPlayers(int totalPlayers) {
        this.totalPlayers = totalPlayers;
    }

    public void setPurse(int purse) {
        this.purse.set(purse);
    }

    public void setRuns(int runs) {
        this.runs.set(runs);
    }
    
        public void setHomeRuns(int value) {
        homeRuns.set(value);
    }

    public void setRunsBattleIn(int runsBattleIn) {
        this.runsBattleIn.set(runsBattleIn);
    }

    public void setStolenBases(int stolenBases) {
        this.stolenBases.set(stolenBases);
    }

    public void setBattingAverage(String battingAverage) {
        this.battingAverage.set(battingAverage);
    }

    public void setWins(int wins) {
        this.wins.set(wins);
    }

    public void setStrikeOuts(int strikeOuts) {
        this.strikeOuts.set(strikeOuts);
    }

    public void setSaves(int saves) {
        this.saves.set(saves);
    }

    public void setEarnRunAverage(String earnRunAverage) {
        this.earnRunAverage.set(earnRunAverage);
    }

    public void setWhip(String whip) {
        this.whip.set(whip);
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints.set(totalPoints);
    }

    public void setPp(int value) {
        pp.set(value);
    }
       public void setTaxiCapacity(int taxiCapacity) {
        this.taxiCapacity = taxiCapacity;
    }


    /**
     ***************************** SETTERS******************************
     */
    /**
     ***************************** PROPERTIES
     *
     ******************************
     * @return
     */
    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty ownerProperty() {
        return owner;
    }

    public IntegerProperty playerNeededProperty() {
        return playerNeeded;
    }

    public IntegerProperty purseProperty() {
        return purse;
    }

    public IntegerProperty runsProperty() {
        return runs;
    }
    
        public IntegerProperty homeRunsProperty() {
        return homeRuns;
    }

    public IntegerProperty runsBattleInProperty() {
        return runsBattleIn;
    }

    public IntegerProperty stolenBasesProperty() {
        return stolenBases;
    }

    public StringProperty battingAverageProperty() {
        return battingAverage;
    }

    public IntegerProperty winsProperty() {
        return wins;
    }

    public IntegerProperty strikeOutsProperty() {
        return strikeOuts;
    }

    public IntegerProperty savesProperty() {
        return saves;
    }

    public StringProperty earnRunAverageProperty() {
        return earnRunAverage;
    }

    public StringProperty whipProperty() {
        return whip;
    }

    public IntegerProperty totalPointsProperty() {
        return totalPoints;
    }

    public IntegerProperty ppProperty() {
        return pp;
    }

    
    /**
     ***************************** PROPERTIES******************************
     */
    
    public void refreshTeamData(){
        recalculate();
    }

    private void recalculate() {
        playerNeeded.set(MAX_PLAYERS_IN_A_TEAM - totalPlayers);

        // MONEY LEFT
        int totalSalary = 0;
        // AGGREGATE OF HITTER STATS
        int RTotal = 0;
        int HRTotal = 0;
        int RBITotal = 0;
        int SBTotal = 0;
        double BATotal = 0;

        // AGGREGATE OF PITCHER STATS
        int WTotal = 0;
        int SVTotal = 0;
        int KTotal = 0;
        int ERATotal = 0;
        int WHIPTotal = 0;

        for (Entry<String, Player> entry : startUpLine.entrySet()) {
            Player p = entry.getValue();
            totalSalary += p.getSalary();

            if (p.getRole().equals(ROLE_HITTER)) {
                RTotal += p.getROrW();
                HRTotal += p.getHrOrSv();
                RBITotal += p.getRbiOrK();
                SBTotal += Integer.valueOf(p.getSbOrEra());
                BATotal += Double.valueOf(p.getBaOrWhip());
            }
            if (p.getRole().equals(ROLE_PITCHER)) {
                WTotal += p.getROrW();
                SVTotal += p.getHrOrSv();
                KTotal += p.getRbiOrK();
                ERATotal += Double.valueOf(p.getSbOrEra());
                WHIPTotal += Double.valueOf(p.getBaOrWhip());
            }
        }
        purse.set(INITIAL_MONEY - totalSalary);

        // MONEY PER PLAYER
        int mp = 0;
        if (playerNeeded.get() != 0) {
            mp = (int) (purse.get() / playerNeeded.get());
        } else {
            mp = -1;
        }
        pp.set(mp);
        
        runs.set(RTotal);
        homeRuns.set(HRTotal);
        runsBattleIn.set(RBITotal);
        stolenBases.set(SBTotal);
        DecimalFormat formater1 = new DecimalFormat("0.000");
        battingAverage.set(formater1.format(BATotal));
        wins.set(WTotal);
        saves.set(SVTotal);
        strikeOuts.set(KTotal);
        DecimalFormat formater2 = new DecimalFormat("0.00");
        earnRunAverage.set(formater2.format(ERATotal));
        whip.set(formater2.format(WHIPTotal));
    }

    public void addPlayer(Player player, String pos) {
        if(totalPlayers < MAX_PLAYERS_IN_A_TEAM){
            String keyName = player.getFirstName() + player.getLastName();
            startUpLine.put(keyName, player);
            int slot = availablePosition.remove(pos);
            availablePosition.put(pos, slot - 1);
            totalPlayers++;
        }
        
        // ADD TO TAXI SQUAD, IN WHICH AUTOMATICALLY ASSIGN X CONTRACT
        else{
            String keyName = player.getFirstName() + player.getLastName();
            taxiSquad.put(keyName, player);
            taxiCapacity--;
        }

        // RECALCULATE STATS
        recalculate();
    }
    
    public void removePlayer(Player playerToRemove){
        String keyName = playerToRemove.getFirstName() + playerToRemove.getLastName();
        if(startUpLine.containsKey(keyName)){
            removeStartUpLinePlayer(playerToRemove);
        }else if(taxiSquad.containsKey(keyName)){
            removeTaxiSquadPlayer(playerToRemove);
        }else{
            
        }
        
    }
    public void removeStartUpLinePlayer(Player player) {
        String keyName = player.getFirstName() + player.getLastName();
        startUpLine.remove(keyName);
        String pos = player.getPosition();
        int slot = availablePosition.remove(pos);
        availablePosition.put(pos, slot + 1);
        totalPlayers--;

// RECALCULATE STATS
        recalculate();
    }
    
    public void removeTaxiSquadPlayer(Player player) {
        String keyName = player.getFirstName() + player.getLastName();
        taxiSquad.remove(keyName);
        taxiCapacity++;

// RECALCULATE STATS
        recalculate();
    }

    //
    public ArrayList<Player> releasePlayers() {
        ArrayList<Player> releasedPlayers = new ArrayList<>();
        
        // RELEASE START LINE
        for (Map.Entry<String, Player> entry : startUpLine.entrySet()) {
            removeStartUpLinePlayer(entry.getValue());
            // RELEASED PLAYER SHOULD ASSIGN TO FREE AGENT POOL
            releasedPlayers.add(entry.getValue());
        }
        
        // RELEASE TAXI
        for (Map.Entry<String, Player> entry : taxiSquad.entrySet()) {
            removeTaxiSquadPlayer(entry.getValue());
            // RELEASED PLAYER SHOULD ASSIGN TO FREE AGENT POOL
            releasedPlayers.add(entry.getValue());
        }
        
        totalPlayers = 0;
        return releasedPlayers;
    }

    public HashMap<String, Integer> getAvailablePosition() {
        return availablePosition;
    }

    public ObservableList<String> getAvailablePositionList() {
        ArrayList<String> res = new ArrayList<>();
        availablePosition.entrySet().stream().filter((entry) -> (entry.getValue() > 0)).forEach((entry) -> {
            res.add(entry.getKey());
        });
        return FXCollections.observableArrayList(res);
    }

    public ObservableList<Player> getPlayersData() {
        ArrayList<Player> players = new ArrayList<>();
        for (Map.Entry<String, Player> entry : startUpLine.entrySet()) {
            players.add(entry.getValue());
        }

        Collections.sort(players, new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                String pos1 = p1.getPosition();
                String pos2 = p2.getPosition();
                if (pos1.equals(pos2)) {
                    return 0;
                }
                switch (pos1) {
                    case POS_C:
                        return -1;
                    case POS_1B:
                        if (pos2.equals(POS_C)) {
                            return 1;
                        }
                    case POS_CI:
                        if (pos2.equals(POS_C) || pos2.equals(POS_1B)) {
                            return 1;
                        }
                    case POS_3B:
                        if (pos2.equals(POS_C) || pos2.equals(POS_1B) || pos2.equals(POS_CI)) {
                            return 1;
                        }
                    case POS_2B:
                        if (pos2.equals(POS_C) || pos2.equals(POS_1B) || pos2.equals(POS_CI) || pos2.equals(POS_3B)) {
                            return 1;
                        }
                    case POS_MI:
                        if (pos2.equals(POS_C) || pos2.equals(POS_1B) || pos2.equals(POS_CI) || pos2.equals(POS_3B) || pos2.equals(POS_2B)) {
                            return 1;
                        }
                    case POS_SS:
                        if (pos2.equals(POS_C) || pos2.equals(POS_1B) || pos2.equals(POS_CI) || pos2.equals(POS_3B) || pos2.equals(POS_2B) || pos2.equals(POS_MI)) {
                            return 1;
                        }
                    case POS_OF:
                        if (pos2.equals(POS_C) || pos2.equals(POS_1B) || pos2.equals(POS_CI) || pos2.equals(POS_3B) || pos2.equals(POS_2B) || pos2.equals(POS_MI) || pos2.equals(POS_SS)) {
                            return 1;
                        }
                    case POS_U:
                        if (pos2.equals(POS_C) || pos2.equals(POS_1B) || pos2.equals(POS_CI) || pos2.equals(POS_3B) || pos2.equals(POS_2B) || pos2.equals(POS_MI) || pos2.equals(POS_SS) || pos2.equals(POS_OF)) {
                            return 1;
                        }
                    case POS_P:
                        if (pos2.equals(POS_C) || pos2.equals(POS_1B) || pos2.equals(POS_CI) || pos2.equals(POS_3B) || pos2.equals(POS_2B) || pos2.equals(POS_MI) || pos2.equals(POS_SS) || pos2.equals(POS_OF) || pos2.equals(POS_U)) {
                            return 1;
                        }
                    default:
                        return -1;
                }
            }
        });
        return FXCollections.observableArrayList(players);
    }
    
    public ObservableList<Player> getTaxiList(){
        ArrayList<Player> players = new ArrayList<>();
        for (Map.Entry<String, Player> entry : taxiSquad.entrySet()) {
            players.add(entry.getValue());
        }
        return FXCollections.observableArrayList(players);
    }

    private SimpleIntegerProperty initIntegerProperty(int n) {
        return new SimpleIntegerProperty(n);
    }

    public boolean isFull() {
        if(totalPlayers == MAX_PLAYERS_IN_A_TEAM && taxiCapacity == 0){
            return true;
        }
        return false;
    }
    
    public boolean isStartUpLineFull(){
        if(totalPlayers == MAX_PLAYERS_IN_A_TEAM)
            return true;
        return false;
    }

}
