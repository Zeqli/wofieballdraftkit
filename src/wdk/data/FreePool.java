/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

/**
 *
 * @author Zeqli
 */
public class FreePool extends Team{

    public FreePool(String teamName, String ownerName) {
        super(teamName, ownerName);
    }
    
    public void addPlayer(Player player) {
        String keyName = player.getFirstName() + player.getLastName();
        startUpLine.put(keyName, player);
    }
    
    
    @Override
    public void removePlayer(Player player) {
        String keyName = player.getFirstName() + player.getLastName();
        startUpLine.remove(keyName);
    }
}
