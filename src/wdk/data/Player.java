/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;
import static wdk.WDK_StartupConstants.PATH_FLAG_IMAGES;
import static wdk.WDK_StartupConstants.PATH_IMAGES;
import static wdk.WDK_StartupConstants.PATH_PLAYER_IMAGES;

/**
 *
 * @author Zeqli
 */
public class Player implements Comparable {

    StringProperty firstName;
    StringProperty lastName;
    StringProperty previousTeam;
    StringProperty position;
    StringProperty positions;
    StringProperty yearOfBirth;
    IntegerProperty rOrW;
    IntegerProperty hrOrSv;
    IntegerProperty rbiOrK;
    StringProperty sbOrEra;
    StringProperty baOrWhip;
    StringProperty contract;
    IntegerProperty salary;
    StringProperty notes;
    StringProperty nationOfBirth;
    String role;
    StringProperty currentTeam;

    Image profileImg;
    Image nationFlagImg;

    // GENERAL PLAYER INFORMATION
    public static final String DEFAULT_PALYER_POSITION = "unkown position";
    public static final String DEFAULT_PALYER_POSITIONS = "<ENTER PLAYER NEW POSITIONS>";
    public static final String DEFAULT_PLAYER_FIRST_NAME = "<ENTER PLAYER FIRST NAME>";
    public static final String DEFAULT_PLAYER_LAST_NAME = "<ENTER PLAYER LAST NAME>";
    public static final String DEFAULT_PALYER_PREVIOUSE_TEAM = "<ENTER PLAYER PREVIOUS TEAM>";
    public static final String DEFAULT_PLAYER_YEAR_OF_BIRTH = "<ENTER YEAR OF BIRTH>";
    public static final String DEFAULT_CONTRACT = "X";
    public static final int DEFAULT_SALARY = 0;
    public static final String DEFAULT_PLAYER_NOTES = "<ENTER NOTES>";
    public static final String DEFAULT_PLAYER_NATION_OF_BIRTH = "unknown";
    public static final String DEFAULT_ROLE = "HITTER";
    public static final String DEFAULT_FANTASY_TEAM = "Free Agent";

    // STATISTIC FOR HITTERS
    public static final String DEFAULT_QUALIFY_POSITION = "<ENTER NEW POSITION>";
    public static final int DEFAULT_RUNS_OR_WINS = 0;
    public static final int DEFAULT_HOME_RUNS_OR_SAVES = 0;
    public static final int DEFAULT_RUNS_BATTLES_IN_OR_STRIKEOUT = 0;
    public static final String DEFAULT_STEAL_BATTLE_OR_EARNED_RUN_AVERAGE = "0";
    public static final String DEFAULT_BAT_AVERAGE_OR_WHIP = "0";

    public static final String DEFAULT_PROFILE_IMAGE_NAME = "AAA_PhotoMissing.jpg";
    public static final String IMAGE_EXTENTION = ".jpg";
    public static final String IMAGE_EXTENTION_PNG = ".png";

    public Player() {
        this.firstName = new SimpleStringProperty(DEFAULT_PLAYER_FIRST_NAME);
        this.lastName = new SimpleStringProperty(DEFAULT_PLAYER_LAST_NAME);
        this.previousTeam = new SimpleStringProperty(DEFAULT_PALYER_PREVIOUSE_TEAM);
        this.position = new SimpleStringProperty(DEFAULT_PALYER_POSITION);
        this.positions = new SimpleStringProperty(DEFAULT_PALYER_POSITIONS);
        this.yearOfBirth = new SimpleStringProperty(DEFAULT_PLAYER_YEAR_OF_BIRTH);
        this.rOrW = new SimpleIntegerProperty(DEFAULT_RUNS_OR_WINS);
        this.hrOrSv = new SimpleIntegerProperty(DEFAULT_HOME_RUNS_OR_SAVES);
        this.rbiOrK = new SimpleIntegerProperty(DEFAULT_RUNS_BATTLES_IN_OR_STRIKEOUT);
        this.sbOrEra = new SimpleStringProperty(DEFAULT_STEAL_BATTLE_OR_EARNED_RUN_AVERAGE);
        this.baOrWhip = new SimpleStringProperty(DEFAULT_BAT_AVERAGE_OR_WHIP);
        this.contract = new SimpleStringProperty(DEFAULT_CONTRACT);
        this.salary = new SimpleIntegerProperty(DEFAULT_SALARY);
        this.notes = new SimpleStringProperty(DEFAULT_PLAYER_NOTES);
        this.nationOfBirth = new SimpleStringProperty(DEFAULT_PLAYER_NATION_OF_BIRTH);
        this.role = DEFAULT_ROLE;
        this.currentTeam = new SimpleStringProperty(DEFAULT_FANTASY_TEAM);
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String value) {
        firstName.set(value);
    }

    public StringProperty FirstNameProperty() {
        return firstName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String value) {
        lastName.set(value);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String getPreviousTeam() {
        return previousTeam.get();
    }

    public void setPreviousTeam(String value) {
        previousTeam.set(value);
    }

    public StringProperty previousTeamProperty() {
        return previousTeam;
    }

    public String getPosition() {
        return position.get();
    }

    public void setPosition(String value) {
        position.set(value);
    }

    public StringProperty positionProperty() {
        return position;
    }

    public String getPositions() {
        return positions.get();
    }

    public void setPositions(String value) {
        positions.set(value);
    }

    public StringProperty positionsProperty() {
        return positions;
    }

    public String getYearOfBirth() {
        return yearOfBirth.get();
    }

    public void setYearOfBirth(String value) {
        yearOfBirth.set(value);
    }

    public StringProperty yearOfBirthProperty() {
        return yearOfBirth;
    }

    public int getROrW() {
        return rOrW.get();
    }

    public void setROrW(int value) {
        rOrW.set(value);
    }

    public IntegerProperty rOrWProperty() {
        return rOrW;
    }

    public int getHrOrSv() {
        return hrOrSv.get();
    }

    public void setHrOrSv(int value) {
        hrOrSv.set(value);
    }

    public IntegerProperty hrOrSvProperty() {
        return hrOrSv;
    }

    public int getRbiOrK() {
        return rbiOrK.get();
    }

    public void setRbiOrK(int value) {
        rbiOrK.set(value);
    }

    public IntegerProperty rbiOrKProperty() {
        return rbiOrK;
    }

    public String getSbOrEra() {
        return sbOrEra.get();
    }

    public void setSbOrEra(String value) {
        sbOrEra.set(value);
    }

    public StringProperty sbOrEraProperty() {
        return sbOrEra;
    }

    public String getBaOrWhip() {
        return baOrWhip.get();
    }

    public void setBaOrWhip(String value) {
        baOrWhip.set(value);
    }

    public StringProperty baOrWhipProperty() {
        return baOrWhip;
    }

    public int getSalary() {
        return salary.get();
    }

    public void setSalary(int value) {
        salary.set(value);
    }

    public IntegerProperty salaryProperty() {
        return salary;
    }

    public String getContract() {
        return contract.get();
    }

    public void setContract(String value) {
        contract.set(value);
    }

    public StringProperty contractProperty() {
        return contract;
    }

    public String getNotes() {
        return notes.get();
    }

    public void setNotes(String value) {
        notes.set(value);
    }

    public StringProperty notesProperty() {
        return notes;
    }

    public String getNationOfBirth() {
        return nationOfBirth.get();
    }

    public void setNationOfBirth(String value) {
        nationOfBirth.set(value);
    }

    public StringProperty notesNationOfBirth() {
        return nationOfBirth;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public String getCurrentTeam() {
        return currentTeam.get();
    }

    public void setCurrentTeam(String value) {
        currentTeam.set(value);
    }

    public StringProperty currentTeamProperty() {
        return currentTeam;
    }
    
    public Image getProfileImage(){
        return profileImg;
    }
    public Image getNationFlag(){
        return nationFlagImg;
    }

    public void loadProfileImage() {
        String profileImageDataPath = PATH_IMAGES + PATH_PLAYER_IMAGES;
        String imagePath = "file:" + profileImageDataPath;
        try{
            profileImg = new Image(imagePath + getLastName()+ getFirstName() + IMAGE_EXTENTION);
        }catch(IllegalArgumentException e){
            System.out.println(e.toString());
            profileImg = new Image(imagePath + DEFAULT_PROFILE_IMAGE_NAME);
        }
    }

    public void loadNationFlagImage() {
        String flagImageDataPath = PATH_IMAGES + PATH_FLAG_IMAGES;
        String imagePath = "file:" + flagImageDataPath;
        nationFlagImg = new Image(imagePath + getNationOfBirth() + IMAGE_EXTENTION_PNG);
    }


    @Override
    public int compareTo(Object obj) {
        Player otherItem = (Player) obj;
        return getFirstName().compareTo(otherItem.getFirstName());
    }

}
