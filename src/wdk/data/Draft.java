/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class represents a draft to be edited and then used to generate a site.
 *
 * @author Zeqing Li
 */
public final class Draft {

    // THESE DRAFT DETAILS DESCRIBE WHAT'S REQUIRED BY
    // THE DRAFT SITE PAGES
    String draftName;
    Team currentTeam;
    FreePool freeAgent;
    ObservableList<Team> allTeams;
    ObservableList<Player> draftSummary;
    

    static final String DEFAULT_DRAFT_NAME = "New Draft";
    static final String FREE_AGENT = "Free Agent";
    static final String FREE_AGENT_OWNER = "Admin";
    static final String DEFAUTL_TEAM = " ";
    static final String DEFAULT_TEAM_OWNER = "";


    /**
     * Constructor for setting up a Course, it initializes the Instructor, which
     * would have already been loaded from a file.
     *
     */
    public Draft() {
        allTeams = FXCollections.observableArrayList();
        draftSummary = FXCollections.observableArrayList();
        draftName = DEFAULT_DRAFT_NAME;
        freeAgent = new FreePool(FREE_AGENT, FREE_AGENT_OWNER);
        // INIT CURRENT TEAM
        currentTeam = new Team(DEFAUTL_TEAM, DEFAULT_TEAM_OWNER);
    }

    public void setDraftName(String draftName) {
        this.draftName = draftName;
    }

    public String getDraftName() {
        if(draftName.isEmpty()){
            draftName = DEFAULT_DRAFT_NAME;
        }
        return draftName;
    }
    
    
    public void setCurrentTeam(Team team) {
        currentTeam = team;
    }

    public Team getCurrentTeam() {
        return currentTeam;
    }

    public ObservableList<Team> getAllTeams() {
        return allTeams;
    }

    public void addTeam(Team draftTeam) {
        allTeams.add(draftTeam);
    }

    public Team getTeam(String teamName) {
        for (Team team : allTeams) {
            if (team.getName().equals(teamName)) {
                return team;
            }
        }
        return freeAgent;
    }

    public Team getFreeAgent() {
        
        return freeAgent;
    }

    public void clearTeams() {
        allTeams.clear();
    }

    public void addPlayerToFreeAgent(Player player) {
        freeAgent.addPlayer(player);
    }

    public void clearFreeAgentPlayers() {
        freeAgent.getPlayersData().clear();
    }

    public boolean isEmpty() {
        if (currentTeam != null) {
            return false;
        }
        return true;
    }

    
    public boolean isInPlayerList(String firstName, String lastName) {
        return freeAgent.getPlayersData().stream().filter((player) -> (player.getFirstName().equals(firstName))).anyMatch((player) -> (player.getLastName().equals(lastName)));
    }


    public void removeTeam(Team teamToRemove) {
        boolean isRemoved = allTeams.remove(teamToRemove);
        if (isRemoved) {
            System.out.println("Team " + teamToRemove.getName() + " Seccesfully Removed.");
        }
    }

    public void deletePlayer(Player playerToBeDelete) {
        freeAgent.removePlayer(playerToBeDelete);
    }

    public void resetCurrentTeam() {
        // IF teamToReomve IS THE ONLY ELEMENT
        if(allTeams.size() == 0){
            currentTeam = new Team(DEFAUTL_TEAM, DEFAULT_TEAM_OWNER);
        }
        // ELSE SET CURRENT TEAM THE FIRST ELEMENT IN THE LIST
        else{
            currentTeam = allTeams.get(0);
        }
    }
    
    // INIT DRAFT SUMMARY
      public ObservableList<Player> initDraftSummaryList(){
        ArrayList<Player> draftSummaryList = new ArrayList<>();
        for (Team team : getAllTeams()) {
            for(Player p : team.getPlayersData()){
                if(p.getContract().equals("S2")){
                    draftSummaryList.add(p);
                }
            }
        }
        draftSummary = FXCollections.observableArrayList(draftSummaryList);
        return draftSummary;
    }  
    
      public void updateDraftSummaryList(Player player){
          if(draftSummary.contains(player) && !player.contract.get().equals("S2")){
              draftSummary.remove(player);
          }
          if(!draftSummary.contains(player) && player.contract.get().equals("S2")){
              draftSummary.add(player);
          }
      }
      
      

    public ObservableList<Player> getDraftSummaryList(){
        return draftSummary;
    }
    
    public void clearDraftSummaryList(){
        draftSummary.clear();
    }
    
    
    public void calcTotalPoints(){
    }

}
