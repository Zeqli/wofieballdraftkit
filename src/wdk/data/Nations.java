/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

/**
 *
 * @author Zeqli
 */
public enum Nations {
    Canada,
    Colombia,
    Cuba,
    Dominican_Republic,
    Germany,
    Japan,
    Mexico,
    Netherlands_Antilles,
    Panama,
    Puerto_Rico,
    South_Korea,
    Taiwan,
    US_Virgin_Islands,
    USA,
    Venezuela
}
