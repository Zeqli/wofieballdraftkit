
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.Collections;
import java.util.Comparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wdk.file.DraftFileManager;

/**
 * This class manages a Draft, which means it knows how to reset one with
 * default values and generate useful dates.
 *
 * @author Zeqing Li
 */
public class DraftDataManager {
    // THIS IS THE DRAFT BEING EDITED
    Draft draft;

    // THIS IS THE UI, WHICH MUST BE UPDATED
    // WHENEVER OUR MODEL'S DATA CHANGES
    DraftDataView view;

    // THIS HELPS US LOAD THINGS FOR OUR DRAFT
    DraftFileManager fileManager;

    // THIS HOLDS ORIGINAL PLAYER DATA
    ObservableList<Player> allPlayers;
    ObservableList<Player> mlbTeamPlayers;

    static final String DEFAULT_MLB_TEAM = "ATL";

    // INITIATE BASIC DATA OF DRAFT, INITIATE DRAFT DATAVIEW, WHICH IS THE GUI CLASS.
    public DraftDataManager(DraftDataView initView) {
        view = initView;
        if (draft == null) {
            draft = new Draft();
        }
        // INIT THE LIST STUFF
        allPlayers = FXCollections.observableArrayList();
        mlbTeamPlayers = FXCollections.observableArrayList();
    }

    /**
     * Accessor method for getting the Draft that this class manages.
     *
     * @return
     */
    public Draft getDraft() {
        return draft;
    }

    /**
     * Accessor method for getting the file manager, which knows how to read and
     * write draft data from/to files.
     */
    public DraftFileManager getFileManager() {
        return fileManager;
    }

    // ADD PLAYERS INTO ALLPLAYER LIST
    public void addPlayer(Player player) {
        String pos = player.getPositions();
        String role = player.getRole();
        if(role.equals("HITTER")){
            if(pos.contains("2B") || pos.contains("SS")){
                pos += "_MI";
            }
            if(pos.contains("1B") || pos.contains("3B")){
                pos += "_CI";
            }
            pos += "_U";
        }
        player.setPositions(pos);
        allPlayers.add(player);
        getMLBTeam();
    }

    public ObservableList<Player> getMLBTeam() {
        return getMLBTeamByName(DEFAULT_MLB_TEAM);
    }

    public ObservableList<Player> getMLBTeamByName(String teamName) {
        mlbTeamPlayers.clear();

        for (Player player : allPlayers) {
            String proTeam = player.getPreviousTeam();
            if (proTeam.equalsIgnoreCase(teamName)) {
                mlbTeamPlayers.add(player);
            }
        }
        Collections.sort(mlbTeamPlayers, new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                String f1 = p1.getFirstName();
                String f2 = p2.getFirstName();
                String l1 = p1.getLastName();
                String l2 = p2.getLastName();

                if (!l1.equals(l2)) {
                    return l1.compareToIgnoreCase(l2);
                } else {
                    return f1.compareToIgnoreCase(f2);
                }
            }

        });
        return mlbTeamPlayers;
    }

    /**
     * Resets the draft to its default initialized settings, triggering the UI
     * to reflect these changes.
     */
    public void reset() {
        // CLEAR ALL THE DRAFT VALUES

        draft.clearFreeAgentPlayers();
        draft.clearTeams();

        // INIT FREE AGENT POOL
        for (Player p : allPlayers) {
            draft.addPlayerToFreeAgent(p);
        }

        // AND THEN FORCE THE UI TO RELOAD THE UPDATED DRAFT
        view.reloadDraft(draft);
    }

    // PRIVATE HELPER METHODS
}
