/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

/**
 *
 * @author Zeqli
 */
public enum WDK_Screen {
    FANTASY_TEAMS,
    PLAYERS,
    FANTASY_STANDINGS,
    DRAFT,
    MLB_TEAM
}
