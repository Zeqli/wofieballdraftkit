/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import wdk.data.Draft;
import wdk.data.Player;
import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_GUI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;
import wdk.data.MLB_National_League_Team;
import wdk.data.Nations;
import static wdk.gui.EditPlayerDialog.COMPLETE;

/**
 *
 * @author Zeqli
 */
public class AddPlayerDialog extends Stage {

    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Player player;

    ArrayList<String> positions;

    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;

    Label firstNameLabel;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;
    Label proPlayerLabel;
    ComboBox proPlayerComboBox;
    Label nationLabel;
    ComboBox nationsBox;
    HBox positionsPane;
    CheckBox C;
    CheckBox firstB;
    CheckBox thirdB;
    CheckBox secondB;
    CheckBox SS;
    CheckBox OF;
    CheckBox P;

    Button completeButton;
    Button cancelButton;

    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;

    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FIRST_NAME_PROMPT = "First Name: ";
    public static final String LAST_NAME_PROMPT = "Last Name: ";
    public static final String PRO_TEAM_PROMPT = "Pro Player: ";
    public static final String NATION_OF_BIRTH_PROMPT = "Nation of Birth: ";
    public static final String ADD_PLAYER_HEADING = "Player Details";
    public static final String ADD_PLAYER_TITLE = "Add New Player Player";
    public static final String C_CHECKBOX = "C";
    public static final String FIRST_BASE_CHECKBOX = "1B";
    public static final String THIRD_BASE_CHECKBOX = "3B";
    public static final String SECOND_BASE_CHECKBOX = "2B";
    public static final String SS_CHECKBOX = "SS";
    public static final String OF_CHECKBOX = "OF";
    public static final String P_CHECKBOX = "P";
    public static final String UNDERSCORE = "_";

    /**
     * Initializes this dialog so that it can be used for either adding new
     * schedule items or editing existing ones.
     *
     * @param primaryStage The ownerLabel of this modal dialog.
     * @param draft
     * @param messageDialog
     */
    public AddPlayerDialog(Stage primaryStage, Draft draft, MessageDialog messageDialog) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(ADD_PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);

        // NOW THE DESCRIPTION 
        firstNameLabel = new Label(FIRST_NAME_PROMPT);
        firstNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        firstNameTextField = new TextField();
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirstName(newValue);
        });

        lastNameLabel = new Label(LAST_NAME_PROMPT);
        lastNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lastNameTextField = new TextField();
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(newValue);
        });

        // AND THE TOPICS
        proPlayerLabel = new Label(PRO_TEAM_PROMPT);
        proPlayerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        ObservableList mlbPlayers = FXCollections.observableArrayList(MLB_National_League_Team.values());
        proPlayerComboBox = new ComboBox(mlbPlayers);
        proPlayerComboBox.setOnAction(e -> {
            player.setPreviousTeam(proPlayerComboBox.getSelectionModel().getSelectedItem().toString());
        });

        // NATION OF BIRTHE SELECTION
        nationLabel = new Label(NATION_OF_BIRTH_PROMPT);
        nationLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        ObservableList nations = FXCollections.observableArrayList(Nations.values());
        nationsBox = new ComboBox(nations);
        nationsBox.selectionModelProperty().addListener((observable, oldValue, newValue) -> {
            player.setNationOfBirth(nationsBox.getSelectionModel().getSelectedItem().toString());
        });

        positionsPane = new HBox();
        positionsPane.setSpacing(4.0);
        C = initChildCheckBox(positionsPane, C_CHECKBOX);
        firstB = initChildCheckBox(positionsPane, FIRST_BASE_CHECKBOX);
        thirdB = initChildCheckBox(positionsPane, THIRD_BASE_CHECKBOX);
        secondB = initChildCheckBox(positionsPane, SECOND_BASE_CHECKBOX);
        SS = initChildCheckBox(positionsPane, SS_CHECKBOX);
        OF = initChildCheckBox(positionsPane, OF_CHECKBOX);
        P = initChildCheckBox(positionsPane, P_CHECKBOX);

        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);

        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button) ae.getSource();
            if (sourceButton.getText().equals(COMPLETE)) {
                if (!hasSelectedAtLeastOnePosition()) {
                    // INCORRECT SELECTION, NOTIFY THE USER
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    messageDialog.show(props.getProperty(WDK_PropertyType.POSITIONS_CHECKBOX_EMTPY_MESSAGE));
                    AddPlayerDialog.this.selection = sourceButton.getText();
                } else if (draft.isInPlayerList(player.getFirstName(), player.getLastName())) {
                    // INCORRECT SELECTION, NOTIFY THE USER
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    messageDialog.show(props.getProperty(WDK_PropertyType.PLAYER_ALREADY_EXIST_MESSAGE));
                    AddPlayerDialog.this.selection = sourceButton.getText();
                    
                } else {
                    player.setPositions(updatePositionsUsingCheckBox());
                    // GET PLAYER IMAGE
                    player.loadProfileImage();
                    player.loadNationFlagImage();
                    AddPlayerDialog.this.selection = sourceButton.getText();
                    AddPlayerDialog.this.hide();
                }
            } else {
                AddPlayerDialog.this.selection = sourceButton.getText();
                AddPlayerDialog.this.hide();
            }
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(firstNameLabel, 0, 1, 1, 1);
        gridPane.add(firstNameTextField, 1, 1, 1, 1);
        gridPane.add(lastNameLabel, 0, 2, 1, 1);
        gridPane.add(lastNameTextField, 1, 2, 1, 1);
        gridPane.add(proPlayerLabel, 0, 3, 1, 1);
        gridPane.add(proPlayerComboBox, 1, 3, 1, 1);
        gridPane.add(nationLabel, 0, 4, 1, 1);
        gridPane.add(nationsBox, 1, 4, 1, 1);
        gridPane.add(positionsPane, 0, 5, 2, 1);
        gridPane.addRow(6, new Text(""));
        gridPane.add(completeButton, 0, 7, 1, 1);
        gridPane.add(cancelButton, 1, 7, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }

    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which button the user
     * selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }

    // USE IN handleAddPlayerRequest and handleEditPlayerRequest in DraftEditController
    public Player getPlayer() {
        return player;
    }

    /**
     * This method loads a custom message into the label and then pops open the
     * dialog.
     *
     * @return
     */
    // Used in handleAddFantasyPlayerRequest
    public Player showAddPlayerDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_PLAYER_TITLE);

        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        player = new Player();

        positions = new ArrayList<>();

        // LOAD THE UI STUFF
        firstNameTextField.setText(player.getFirstName());
        lastNameTextField.setText(player.getLastName());
        proPlayerComboBox.getSelectionModel().select(0);
        nationsBox.getSelectionModel().select(0);

        // AND OPEN IT UP
        this.showAndWait();

        return player;
    }

    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }

    /**
     * **************helper*************************
     */
    private CheckBox initChildCheckBox(Pane container, String label) {
        CheckBox cb = new CheckBox(label);
        container.getChildren().add(cb);
        return cb;
    }

    private String updatePositionsUsingCheckBox() {
        if (C.isSelected()) {
            positions.add(C_CHECKBOX);
        }
        if (firstB.isSelected()) {
            positions.add(FIRST_BASE_CHECKBOX);
        }
        if (thirdB.isSelected()) {
            positions.add(THIRD_BASE_CHECKBOX);
        }
        if (secondB.isSelected()) {
            positions.add(SECOND_BASE_CHECKBOX);
        }
        if (SS.isSelected()) {
            positions.add(SS_CHECKBOX);
        }
        if (OF.isSelected()) {
            positions.add(OF_CHECKBOX);
        }
        if (P.isSelected()) {
            positions.add(P_CHECKBOX);
        }
        String pos = "";
        for (String s : positions) {
            pos += s + UNDERSCORE;
        }
        
        if(pos.contains("2B") || pos.contains("SS")){
            pos += "MI_";
        }
        if(pos.contains("1B") || pos.contains("3B")){
            pos += "CI_";
        }
        pos += "U_";
        pos = pos.substring(0, pos.length()-2);
        return pos;
    }

    private boolean hasSelectedAtLeastOnePosition() {
        if (C.isSelected()) {
            return true;
        }
        if (firstB.isSelected()) {
            return true;
        }
        if (thirdB.isSelected()) {
            return true;
        }
        if (secondB.isSelected()) {
            return true;
        }
        if (SS.isSelected()) {
            return true;
        }
        if (OF.isSelected()) {
            return true;
        }
        if (P.isSelected()) {
            return true;
        }
        return false;
    }
}
