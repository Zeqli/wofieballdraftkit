/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import wdk.data.Draft;
import wdk.data.Player;
import wdk.data.Team;
import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_GUI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_GUI.CLASS_SUBHEADING_LABEL;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;

/**
 *
 * @author Zeqli
 */
public class EditPlayerDialog extends Stage {

    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Player player;
    String selectedTeam;
    String selectedPosition;
    String selectedContract;
    String salary;

    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;

    // IMAGES
    ImageView playerImgView;
    ImageView flagImgView;

    Label nameLabel;
    Label positionsLabel;
    Label fantasyTeamLabel;
    ComboBox fantasyTeamBox;
    Label positionLabel;
    ComboBox positionBox;
    Label contractLabel;
    ComboBox contractBox;
    Label salaryLabel;
    TextField salaryTextField;

    Button completeButton;
    Button cancelButton;

    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;

    // CONSTANTS FOR OUR UI
    static final String COMPLETE = "Complete";
    static final String CANCEL = "Cancel";
    static final String FANTASY_TEAM_PROMPT = "Fantasy Team: ";
    static final String POSITION_PROMPT = "Position: ";
    static final String CONTRACT_PROMPT = "Contract: ";
    static final String SALARY_PROMPT = "Salary ($): ";
    static final String EDIT_PLAYER_HEADING = "Player Details";
    static final String EDIT_PLAYER_TITLE = "Edit Player";
    static final String IMAGE_EXTENTIONS = ".jpg";
    static final String SPACE = " ";
    static final String FREE_AGENT = "Free Agent";
    static final String TAXI_SQUAD = "Taxi Squad";

    /**
     * Initializes this dialog so that it can be used for either adding new
     * schedule items or editing existing ones.
     *
     * @param primaryStage The ownerLabel of this modal dialog.
     * @param draft
     * @param playerToEdit
     * @param messageDialog
     */
    public EditPlayerDialog(Stage primaryStage, Draft draft, MessageDialog messageDialog) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(EDIT_PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);

//         NOW THE IMAGES
        playerImgView = new ImageView();
        flagImgView = new ImageView();

        // NOW THE PLAYER DESCRIPTION 
        nameLabel = new Label();
        nameLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        positionsLabel = new Label();
        positionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);

        // AND THE FANTASY TEAM
        fantasyTeamLabel = new Label(FANTASY_TEAM_PROMPT);
        fantasyTeamBox = new ComboBox();

        // AND THE POSITION
        positionLabel = new Label(POSITION_PROMPT);
        positionBox = new ComboBox();

        // AND THE CONTRACT
        contractLabel = new Label(CONTRACT_PROMPT);
        ObservableList contracts = FXCollections.observableArrayList(Contracts.values());
        contractBox = new ComboBox(contracts);

        // AND THE SALARY
        salaryLabel = new Label(SALARY_PROMPT);
        salaryTextField = new TextField();

        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);

        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button) ae.getSource();
            if (sourceButton.getText().equals(COMPLETE)) {
                if (salary.isEmpty()) {
                    // INCORRECT SELECTION, NOTIFY THE USER
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
//                    messageDialog.show(props.getProperty(WDK_PropertyType.NO_SALARY_MESSAGE));

                } else if (selectedPosition.isEmpty()) {
                    // INCORRECT SELECTION, NOTIFY THE USER
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    messageDialog.show(props.getProperty(WDK_PropertyType.POSITION_SELECTION_MISSING_MESSAGE));
                } else {
                    
                    EditPlayerDialog.this.hide();

                }EditPlayerDialog.this.selection = sourceButton.getText();
            } else {
                EditPlayerDialog.this.selection = sourceButton.getText();
                EditPlayerDialog.this.hide();

            }

        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(playerImgView, 0, 1, 1, 5);
        gridPane.add(flagImgView, 1, 1, 1, 1);
        gridPane.add(nameLabel, 1, 2, 1, 1);
        gridPane.add(new Text(""), 1, 3, 1, 1);
        gridPane.add(positionsLabel, 1, 4, 1, 1);
        gridPane.add(new Text(""), 1, 5, 1, 1);
        gridPane.add(fantasyTeamLabel, 0, 6, 1, 1);
        gridPane.add(fantasyTeamBox, 1, 6, 1, 1);
        gridPane.add(positionLabel, 0, 7, 1, 1);
        gridPane.add(positionBox, 1, 7, 1, 1);
        gridPane.add(contractLabel, 0, 8, 1, 1);
        gridPane.add(contractBox, 1, 8, 1, 1);
        gridPane.add(salaryLabel, 0, 9, 1, 1);
        gridPane.add(salaryTextField, 1, 9, 1, 1);
        gridPane.add(completeButton, 0, 10, 1, 1);
        gridPane.add(cancelButton, 1, 10, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }

    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which button the user
     * selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }

    public Player getPlayer() {
        player.setCurrentTeam(selectedTeam);
        player.setPosition(selectedPosition);
        player.setContract(selectedContract);
        player.setSalary(Integer.valueOf(salary));
        return player;
    }

    // Used in handleEditTeamRequest
    public void loadGUIData(Draft draft, Player playerToEdit, WDK_Screen screen ) {
        //NOW THE IMAGES
        playerImgView.setImage(playerToEdit.getProfileImage());
        flagImgView.setImage(playerToEdit.getNationFlag());

        // NOW THE PLAYER DESCRIPTION 
        nameLabel.setText(playerToEdit.getFirstName() + SPACE + playerToEdit.getLastName());
        nameLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        positionsLabel.setText(playerToEdit.getPositions());

        updataPositionComboBox(draft, playerToEdit, screen);

        updataFantasyTeamComboBox(draft, screen);
        
        // LOAD THE UI STUFF
        fantasyTeamBox.getSelectionModel().select(selectedTeam);
        positionBox.getSelectionModel().select(selectedPosition);
        contractBox.getSelectionModel().select(selectedContract);
        salaryTextField.setText(String.valueOf(salary));
    }

    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }

    public void showEditPlayerDialog(Draft draft, Player playerToEdit, WDK_Screen screen) {
        if (playerToEdit == null) {
            return;
        }
        // SET THE DIALOG TITLE
        setTitle(EDIT_PLAYER_TITLE);

        player = new Player();

        // LOAD THE PLAYER INTO OUR LOCAL OBJECT
        selectedTeam = playerToEdit.getCurrentTeam();
        selectedPosition = playerToEdit.getPosition();
        selectedContract = playerToEdit.getContract();
        salary = String.valueOf(playerToEdit.getSalary());
        initHandler(draft, playerToEdit, screen);

        // AND THEN INTO OUR GUI
        loadGUIData(draft, playerToEdit, screen);

        // AND OPEN IT UP
        this.showAndWait();
    }

    private void initHandler(Draft draft, Player playerToEdit, WDK_Screen screen) {
        fantasyTeamBox.setOnAction(e -> {
            selectedTeam = fantasyTeamBox.getSelectionModel().getSelectedItem().toString();
            updataPositionComboBox(draft, playerToEdit, screen);
        });

        positionBox.setOnAction(e -> {
            if (positionBox != null && positionBox.getValue() != null) {
                selectedPosition = positionBox.getSelectionModel().getSelectedItem().toString();
            }
        });

        contractBox.setOnAction(e -> {
            selectedContract = contractBox.getSelectionModel().getSelectedItem().toString();
        });

        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            salary = newValue;
        });
    }

    
    // UPDATE POSITION COMBOBOX AND MAKE IT SELECT THE FIRST ELEMENT 
    // WHEN THERE IS NO SELECTION AVAILABLE IN POSITION BOX, SET COMPLETE BUTTON DISABLE
    private void updataPositionComboBox(Draft draft, Player playerToEdit, WDK_Screen screen) {
        // NOW THE POSITION COMBOBOX
        ArrayList<String> positions = new ArrayList<>();
        draft.getTeam(selectedTeam).getAvailablePositionList().stream().forEach((pos) -> {
            positions.add(pos);
        });
        ArrayList<String> availablePos = new ArrayList<>();
        for (String pos : positions) {
            if (playerToEdit.getPositions().contains(pos)) {
                availablePos.add(pos);
            }
        }
        
        // IF TAXI SQUAD AVAILABLE
        if(draft.getTeam(selectedTeam).isStartUpLineFull() && !draft.getTeam(selectedTeam).isFull() && screen == WDK_Screen.PLAYERS ){
            availablePos.add(TAXI_SQUAD);
        }
        if(screen == WDK_Screen.FANTASY_TEAMS){
            availablePos.add(playerToEdit.getPosition());
        }
        positionBox.setItems(FXCollections.observableArrayList(availablePos));
        if(availablePos.isEmpty()){
            completeButton.setDisable(true);
            positionBox.setDisable(true);
        }else{
            positionBox.getSelectionModel().select(0);
            completeButton.setDisable(false);
            positionBox.setDisable(false);
        }
    }

    private void updataFantasyTeamComboBox(Draft draft, WDK_Screen screen) {
        ArrayList<String> teams = new ArrayList<>();
        for (Team team : draft.getAllTeams()) {
            if(!team.isFull()){
            teams.add(team.getName());
            }
        }
        teams.add(draft.getFreeAgent().getName());
        
        fantasyTeamBox.setItems(FXCollections.observableArrayList(teams));
    }
}
