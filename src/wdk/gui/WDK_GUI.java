package wdk.gui;

import static wdk.WDK_StartupConstants.*;
import static wdk.gui.WDK_Screen.*;
import wdk.WDK_PropertyType;
import wdk.controller.DraftEditController;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.DraftDataView;
import wdk.controller.FileController;
import wdk.file.DraftFileManager;
import wdk.file.DraftSiteExporter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.Predicate;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.controller.FantasyTeamEditController;
import wdk.data.MLB_National_League_Team;
import wdk.data.Nations;
import wdk.data.Player;
import wdk.data.Team;

/**
 * This class provides the Graphical User Interface for this application,
 * managing all the UI components for editing a Draft and exporting it to a
 * site.
 *
 * @author Zeqing Li
 */
public class WDK_GUI implements DraftDataView {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_RADIO_PANE = "radio_pane";
    static final String CLASS_PLAYER_TOOLBAR_PANE = "player_toolbar_pane";
    static final String EMPTY_TEXT = "";
    static final String DASH = "-";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    static final int CENTER_PANE_WIDTH = 400;
    static final int CENTER_PANE_HEIGHT = 550;
    static final int POSITION_SELECTION_PANE_WIDTH = CENTER_PANE_WIDTH;
    static final int POSITION_SELECTION_PANE_HEIGHT = 40;

    // THIS MANAGES ALL OF THE APPLICATION'S DATA
    DraftDataManager dataManager;

    // THIS MANAGES DRAFT FILE I/O
    DraftFileManager draftFileManager;

    // THIS MANAGES EXPORTING OUR SITE PAGES
    DraftSiteExporter siteExporter;

    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;

    // THIS HANDLES INTERACTIONS WITH DRAFT INFO CONTROLS
    DraftEditController draftController;

    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane wdkPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exportSiteButton;
    Button exitButton;

    // THIS IS THE BOTTOM TOOLBAR AND ITS CONTROLS
    FlowPane ScreenToolbarPane;
    Button fantasyTeamsButton;
    Button playersButton;
    Button fantasyStandingsButton;
    Button draftButton;
    Button mlbTeamsButton;

    // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
    BorderPane workspacePane;
    boolean workspaceActivated;

    // WE'LL PUT THE WORKSPACE INSIDE A SCROLL PANE
    ScrollPane workspaceScrollPane;

    // WE'LL PUT THIS IN THE TOP OF THE WORKSPACE, IT WILL
    // HOLD TWO OTHER PANES FULL OF CONTROLS AS WELL AS A LABEL
    BorderPane centerWorkspacePane;
    Label ScreenHeadingLabel;
    SplitPane centerWorkspaceSplitPane;

    // THESE ARE THE SCREENS PANE
    BorderPane fantasyTeamsPane;
    BorderPane playerPane;
    BorderPane fantasyStandingsPane;
    BorderPane draftPane;
    BorderPane mlbTeamsPane;

    // THESE ARE THE CONTROLS AND LABELS FOR SCREENS
    // FANTASY TEAMS CONTROLS
    Label fantasyTeamsHeadingLabel;

    // TODO
    // FANTASY TEAM SCROLL PANE
    HBox draftNameToolbar;
    // DRAFT NAME LABEL
    // DRAFT NAME TEXTFIELD
    Label draftNameLabel;
    TextField draftNameTextField;

    // ADD TEAM BUTTON
    // REMOVE TEAM BUTTON 
    // EDIT TEAM BUTTON 
    HBox fantasyTeamToolbar;
    Button addFantasyTeamButton;
    Button removeFantasyTeamButton;
    Button editFantasyTeamButton;

    // FANTEAM SELECTION LABEL
    // FANTEAM SELECTION COMBOBOX
    Label fantasyTeamSelectionLabel;
    ComboBox<String> fantasyTeamSelectionBox;

    // STARTING LINEUP PANE
    // STARTING LINEUP LABEL
    // STARTING LINEUP TABLE
    VBox slPane;
    Label startLineupLabel;

    // TAXI SQUAD PANE
    // TAXI SQUAD LABEL
    // TAXI SQUAD TABLE
    VBox tsPane;
    Label taxiSquadLabel;

    // PLAYERS SCREEN CONTROLS
    Label playerHeadingLabel;
    VBox playersBox;
    HBox playersToolbar;
    Button deletePlayerButton;
    Button addPlayerButton;
    Label searchPlayerLabel;
    TextField searchPlayerTextField;
    
    
    // DRAFT SUMMARY CONTROLS
    FlowPane draftControlPane;
    Button selectPlayerButton;
    Button startAutoDraft;
    Button pauseAutoDraft;

    // PLAYER TABLE OPTIONS
    HBox positionSelectionBar;
    RadioButton allButton;
    RadioButton cButton;
    RadioButton onebButton;
    RadioButton ciButton;
    RadioButton threebButton;
    RadioButton twobButton;
    RadioButton miButton;
    RadioButton ssButton;
    RadioButton ofButton;
    RadioButton uButton;
    RadioButton pButton;
    ToggleGroup radioGroup;

    static final String RADIOBUTTON_LABEL_ALL = "ALL";
    static final String RADIOBUTTON_LABEL_C = "C";
    static final String RADIOBUTTON_LABEL_1B = "1B";
    static final String RADIOBUTTON_LABEL_CI = "CI";
    static final String RADIOBUTTON_LABEL_3B = "3B";
    static final String RADIOBUTTON_LABEL_2B = "2B";
    static final String RADIOBUTTON_LABEL_MI = "MI";
    static final String RADIOBUTTON_LABEL_SS = "SS";
    static final String RADIOBUTTON_LABEL_OF = "OF";
    static final String RADIOBUTTON_LABEL_U = "U";
    static final String RADIOBUTTON_LABEL_P = "P";

    // FANTASY STANDINGS CONTROLS
    Label fantasyStandingsHeadingLabel;

    // DRAFT CONTROLS
    Label draftHeadingLabel;

    // MLB TEAMS CONTROLS
    Label mlbTeamsHeadingLabel;
    Label selectProTeamLabel;
    ComboBox selectProTeamBox;
    FlowPane selectProTeamFlowPane;

    /**
     * *************************TABLES*******************************
     */
    // FANTASY TEAMS TABLE
    TableView<Player> startLineupTable;

    // FANTASY TEAMS COLUMNS
    TableColumn tFirstColumn;
    TableColumn tLastColumn;
    TableColumn tProTeamColumn;
    TableColumn tPositionColumn;
    TableColumn tPositionsColumn;
    TableColumn tYearOfBirthColumn;
    TableColumn tROrWColumn;
    TableColumn tHrOrSvColumn;
    TableColumn tRbiOrKColumn;
    TableColumn tSbOrEraColumn;
    TableColumn tBaOrWhipColumn;
    TableColumn tEstimatedValueColumn;
    TableColumn tNotesColumn;
    TableColumn tContractColumn;
    TableColumn tSalaryColumn;

    // TAXI SQUAD TABLE
    TableView<Player> taxiSquadTable;

    TableColumn sTaxiColumn;
    TableColumn sFirstColumn;
    TableColumn sLastColumn;
    TableColumn sProTeamColumn;
    TableColumn sPositionsColumn;
    TableColumn sYearOfBirthColumn;
    TableColumn sROrWColumn;
    TableColumn sHrOrSvColumn;
    TableColumn sRbiOrKColumn;
    TableColumn sSbOrEraColumn;
    TableColumn sBaOrWhipColumn;
    TableColumn sEstimatedValueColumn;
    TableColumn sNotesColumn;
    TableColumn sContractColumn;
    TableColumn sSalaryColumn;

    // AVAILABLE PLAYERS TABLE
    TableView<Player> playerTable;

    // AVAILABLE PLAYERS TABLE COLUMNS
    TableColumn pFirstColumn;
    TableColumn pLastColumn;
    TableColumn pProTeamColumn;
    TableColumn pPositionsColumn;
    TableColumn pYearOfBirthColumn;
    TableColumn pROrWColumn;
    TableColumn pHrOrSvColumn;
    TableColumn pRbiOrKColumn;
    TableColumn pSbOrEraColumn;
    TableColumn pBaOrWhipColumn;
    TableColumn pEstimatedValueColumn;
    TableColumn pNotesColumn;
    TableColumn pContractColumn;
    TableColumn pSalaryColumn;
    TableColumn pTaxiColumn;

    // FANTASY STANDINGS TABLE
    TableView<Team> fantasyStandingTable;

    TableColumn fTeamNameColumn;
    TableColumn fPlayerNeededColumn;
    TableColumn fMoneyLeftColumn;
    TableColumn fMoneyPerPersonColumn;
    TableColumn fRTotalColumn;
    TableColumn fHRTotalColumn;
    TableColumn fRBITotalColumn;
    TableColumn fSBTotalColumn;
    TableColumn fBATotalColumn;
    TableColumn fWTotalColumn;
    TableColumn fSVTotalColumn;
    TableColumn fKTotalColumn;
    TableColumn fERATotalColumn;
    TableColumn fWHIPTotalColumn;
    TableColumn fTotalPointsColumn;

    // FANTASY STANDINGS COLUMNS
    // DRAFT SUMMARY TABLE
    TableView<Player> draftSummaryTable;

    TableColumn dPickNumberColumn;
    TableColumn dFirstNameColumn;
    TableColumn dLastNameColumn;
    TableColumn dTeamNameColumn;
    TableColumn dContractColumn;
    TableColumn dSalaryColumn;

    // DRAFT SUMMARY COLUMNS
    // MLB TEAMS TABLE
    TableView<Player> mlbTeamsTable;
    // MLB TEAMS COLUMNS
    TableColumn mFirstColumn;
    TableColumn mLastColumn;
    TableColumn mPositionsColumn;

    /**
     * *************************TABLES*******************************
     */
    // AND TABLE COLUMNS
    static final String COL_DESCRIPTION = "Description";
    static final String COL_DATE = "Date";
    static final String COL_LINK = "Link";
    static final String COL_TOPIC = "Topic";
    static final String COL_SESSIONS = "Number of Sessions";
    static final String COL_NAME = "Name";
    static final String COL_TOPICS = "Topics";

    static final String COL_FIRST = "First";
    static final String COL_LAST = "Last";
    static final String COL_POSITION = "Position";
    static final String COL_POSITIONS = "Positions";
    static final String COL_PRO_TEAM = "Pro Team";
    static final String COL_YEAR_OR_BIRTH = "Year of Birth";
    static final String COL_R_OR_W = "R/W";
    static final String COL_HR_OR_SV = "HR/SV";
    static final String COL_RBI_OR_K = "RBI/K";
    static final String COL_SB_OR_ERA = "SB/ERA";
    static final String COL_BA_OR_WHIP = "BA/WHIP";
    static final String COL_EST_VALUE = "Estimated Value";
    static final String COL_NOTES = "Notes";
    static final String COL_CONTRACT = "Contract";
    static final String COL_SALARY = "Salary";
    static final String COL_TAXI = "Taxi";

    static final String COL_TEAM_NAME = "Team Name";
    static final String COL_PLAYERS_NEEDED = "Player Needed";
    static final String COL_MONEY_LEFT = "$ Left";
    static final String COL_MONEY_PER_PERSON = "$ PP";
    static final String COL_R = "R";
    static final String COL_HR = "HR";
    static final String COL_RBI = "RBI";
    static final String COL_SB = "SB";
    static final String COL_BA = "BA";
    static final String COL_W = "W";
    static final String COL_SV = "SV";
    static final String COL_K = "K";
    static final String COL_ERA = "ERA";
    static final String COL_WHIP = "WHIP";
    static final String COL_TOTAL_POINTS = "Total Points";
    static final String COL_PICK_NUMBER = "Pick #";
    static final String COL_TEAM = "Team";
    static final String COL_SALARY_DOLLAR = "Salary ($)";

    static final String POS_C = "C";
    static final String POS_1B = "1B";
    static final String POS_CI = "CI";
    static final String POS_3B = "3B";
    static final String POS_2B = "2B";
    static final String POS_MI = "MI";
    static final String POS_SS = "SS";
    static final String POS_OF = "OF";
    static final String POS_U = "U";
    static final String POS_P = "P";

    // HERE ARE OUR DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    VBox ftCentralPane;
    VBox pCentralPane;
    VBox fsCentralPane;
    VBox mlbtCentralPane;
    VBox dCentralPane;

    ;

    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initGUI.
     *
     * @param initPrimaryStage Window inside which the GUI will be displayed.
     */
    public WDK_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }

    WDK_GUI() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Accessor method for the data manager.
     *
     * @return The DraftDataManager used by this UI.
     */
    public DraftDataManager getDataManager() {
        return dataManager;
    }

    /**
     * Accessor method for the file controller.
     *
     * @return The FileController used by this UI.
     */
    public FileController getFileController() {
        return fileController;
    }

    /**
     * Accessor method for the draft file manager.
     *
     * @return The DraftFileManager used by this UI.
     */
    public DraftFileManager getDraftFileManager() {
        return draftFileManager;
    }

    /**
     * Accessor method for the site exporter.
     *
     * @return The DraftSiteExporter used by this UI.
     */
    public DraftSiteExporter getSiteExporter() {
        return siteExporter;
    }

    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }

    public MessageDialog getMessageDialog() {
        return messageDialog;
    }

    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }

    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The DraftDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }

    /**
     * Mutator method for the draft file manager.
     *
     * @param initDraftFileManager The DraftFileManager to be used by this UI.
     */
    public void setDraftFileManager(DraftFileManager initDraftFileManager) {
        draftFileManager = initDraftFileManager;
    }

    /**
     * Mutator method for the site exporter.
     *
     * @param initSiteExporter The DraftSiteExporter to be used by this UI.
     */
    public void setSiteExporter(DraftSiteExporter initSiteExporter) {
        siteExporter = initSiteExporter;
    }

    /**
     * This method fully initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the UI window's title bar.
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initGUI(String windowTitle) throws IOException {
        // INIT THE DIALOGS
        initDialogs();

        // INIT THE TOOLBAR
        initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }

    /**
     * When called this function puts the workspace into the window, revealing
     * the controls for editing a Draft.
     */
    public void activateWorkspace() {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            wdkPane.setCenter(workspacePane);
            workspaceActivated = true;
        }
    }

    /**
     * This function takes all of the data out of the draftToReload argument and
     * loads its values into the user interface controls. Called when load Draft
     * from file or create new Draft.
     *
     * @param draftToReload The Draft whose data we'll load into the GUI.
     */
    @Override
    public void reloadDraft(Draft draftToReload) {
        // FIRST ACTIVATE THE WORKSPACE IF NECESSARY
        if (!workspaceActivated) {
            activateWorkspace();
        }

        // WE DON'T WANT TO RESPOND TO EVENTS FORCED BY
        // OUR INITIALIZATION SELECTIONS
        draftController.enable(false);

        if (!draftToReload.isEmpty()) {
//            fantasyTeamSelectionBox.setValue(draftToReload.getCurrentTeam().getName());
            fantasyTeamSelectionBox.setDisable(false);
        } else {
            fantasyTeamSelectionBox.setValue(null);
            fantasyTeamSelectionBox.setDisable(true);
        }
        draftNameTextField.setText("" + draftToReload.getDraftName());

        // CLEAR STARTLINEUP TABLE
        startLineupTable.setItems(FXCollections.observableArrayList(new ArrayList<Player>()));
        if (!draftToReload.getCurrentTeam().getName().endsWith(draftToReload.getFreeAgent().getName())) {
            fantasyTeamSelectionBox.getSelectionModel().select(draftToReload.getCurrentTeam().getName());
            startLineupTable.setItems(draftToReload.getCurrentTeam().getPlayersData());
            taxiSquadTable.setItems(draftToReload.getCurrentTeam().getTaxiList());
        }

        reloadFantasyTeamScreenControls();
        // SET DEFAULT SELECTED TEAM AS FREE AGENT, BUT DO NOT SHOW IT'S CONTENT ON STARTUP LINE
        draftToReload.resetCurrentTeam();
        
        draftToReload.clearDraftSummaryList();
        draftSummaryTable.setItems(draftToReload.initDraftSummaryList());
//        startLineupTable.setItems(draftToReload.getCurrentTeam().getPlayersData());
//
//        // FORCE IT SORT
        startLineupTable.getSortOrder().add(tPositionColumn);
        startLineupTable.sort();

        // FANTASY STANDING TABLE
        fantasyStandingTable.setItems(draftToReload.getAllTeams());

        // AVAILABLE PLAYER TABLE
        // MLB TEAM TABLE
        selectProTeamBox.setItems(FXCollections.observableArrayList(MLB_National_League_Team.values()));
        selectProTeamBox.getSelectionModel().select(0);
        mlbTeamsTable.setItems(dataManager.getMLBTeam());
        initFilteredData();

        // NOW WE DO WANT TO RESPOND WHEN THE USER INTERACTS WITH OUR CONTROLS
        draftController.enable(true);
    }

    /**
     * This method is used to activate/deactivate toolbar buttons when they can
     * and cannot be used so as to provide foolproof design.
     *
     * @param saved Describes whether the loaded Draft has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT DRAFT
        // HAS BEEN SAVED OR NOT
        saveDraftButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST DRAFT BEGINS
        loadDraftButton.setDisable(saved);
        exportSiteButton.setDisable(saved);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    public void changeScreen(WDK_Screen screen) {
        centerWorkspacePane.setVisible(false);
        switch (screen) {
            case FANTASY_TEAMS:
                reloadFantasyTeamScreenControls();
                fantasyTeamsPane.setVisible(true);
                centerWorkspacePane = fantasyTeamsPane;
                break;
            case PLAYERS:
                reloadPlayerScreenControls();
                playerPane.setVisible(true);
                centerWorkspacePane = playerPane;
                break;
            case FANTASY_STANDINGS:
                fantasyStandingsPane.setVisible(true);
                centerWorkspacePane = fantasyStandingsPane;
                break;
            case DRAFT:
                draftPane.setVisible(true);
                centerWorkspacePane = draftPane;
                break;
            case MLB_TEAM:
                mlbTeamsPane.setVisible(true);
                centerWorkspacePane = mlbTeamsPane;
                break;
            default:
                break;
        }
        workspaceScrollPane.setContent(centerWorkspacePane);
    }

    /**
     * This function loads all the values currently in the user interface into
     * the draft argument.
     *
     * @param draft The draft to be updated using the data from the UI controls.
     */
    public void updateDraftInfo(Draft draft) {
        // RESET TABLE
        startLineupTable.setItems(FXCollections.observableArrayList(new ArrayList<Player>()));
        startLineupTable.setItems(draft.getCurrentTeam().getPlayersData());

        taxiSquadTable.setItems(FXCollections.observableArrayList(new ArrayList<Player>()));
        taxiSquadTable.setItems(draft.getCurrentTeam().getTaxiList());

        // GET UPDATED DRAFT SUMMARY LIST
        draftSummaryTable.setItems(draft.getDraftSummaryList());
        
        
        
        initFilteredData();

        loadFantasyTeamSelctionComboBox(draft.getAllTeams());
    }

    /**
     * *************************************************************************
     */
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /**
     * *************************************************************************
     */
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        // FILE TOOL BAR IN THE TOP REGION
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.NEW_DRAFT_ICON, WDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.LOAD_DRAFT_ICON, WDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.SAVE_DRAFT_ICON, WDK_PropertyType.SAVE_DRAFT_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXPORT_PAGE_ICON, WDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false);

        // FILE TOOL BAR IN THE BOTTOM REGION
        ScreenToolbarPane = new FlowPane();
        fantasyTeamsButton = initChildButton(ScreenToolbarPane, WDK_PropertyType.FANTASY_TEAMS_ICON, WDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        playersButton = initChildButton(ScreenToolbarPane, WDK_PropertyType.PLAYERS_ICON, WDK_PropertyType.PLAYERS_SCREEN_TOOLTIP, false);
        fantasyStandingsButton = initChildButton(ScreenToolbarPane, WDK_PropertyType.FANTASY_STANDING_ICON, WDK_PropertyType.FANTASY_STANDING_TOOLTIP, false);
        draftButton = initChildButton(ScreenToolbarPane, WDK_PropertyType.DRAFT_SUMMARY_ICON, WDK_PropertyType.DRAFT_SUMMARY_TOOLTIP, false);
        mlbTeamsButton = initChildButton(ScreenToolbarPane, WDK_PropertyType.MLB_TEAM_ICON, WDK_PropertyType.MLB_TEAM_TOOLTIP, false);

    }

    // CREATES AND SETS UP ALL THE CONTROLS TO GO IN THE APP WORKSPACE
    private void initWorkspace() throws IOException {
        // THE CENTER WORKSPACE HOLDS THE CHANGABLE SCREENS
        initCenterWorkspace();

        // AND NOW PUT IT IN THE WORKSPACE
        workspaceScrollPane = new ScrollPane();
        workspaceScrollPane.setContent(centerWorkspacePane);
        workspaceScrollPane.setFitToWidth(true);

//        workspaceScrollPane.getStyleClass().add(CLASS_BORDERED_PANE);
        // THIS IS FOR MANAGING SCHEDULE EDITING
        //initScheduleItemsControls();
        // THIS HOLDS ALL OUR WORKSPACE COMPONENTS, SO NOW WE MUST
        // ADD THE COMPONENTS WE'VE JUST INITIALIZED
        workspacePane = new BorderPane();
        workspacePane.setCenter(workspaceScrollPane);
        workspacePane.setBottom(ScreenToolbarPane);
        workspacePane.getStyleClass().add(CLASS_BORDERED_PANE);

        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // DRAFT OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;
    }

    // INITIALIZES THE TOP PORTION OF THE WORKWPACE UI
    private void initCenterWorkspace() {
        // INIT SCREENS GUI
        initScreens();

        // THE TOP WORKSPACE PANE WILL ONLY DIRECTLY HOLD 2 THINGS, A LABEL
        // AND A SPLIT PANE, WHICH WILL HOLD 2 ADDITIONAL GROUPS OF CONTROLS
        centerWorkspacePane = fantasyTeamsPane;

    }

    // INIT FIVE SCREENS
    private void initScreens() {
        initTables();
        initFantasyTeamScreen();
        initPlayersScreen();
        initFantasyStadingsScreen();
        initDraftScreen();
        initMLBTeamsScreen();
    }

    // INITIALIZE TABLE COLUMNS.
    private void initTables() {
        initFantasyTeamsTable();
        initTaxiSquadTable();
        initAvailablePlayersTable();
        initFantasyStandingsTable();
        initDraftSummaryTable();
        initMLBTeamsTable();

    }

    // INITIALIZE THE FANTASY TEAM PANE
    private void initFantasyTeamScreen() {
        fantasyTeamsPane = new BorderPane();

        // BODY OF THE FANTASY TEAMS SCREEN. 
        // INCLUDE 
        // 1. DRAFT NAME FIELD
        // 2. ADD/REMOVE FANTASY TEAM BUTTONS AND FANTASY AND TEAM SELECTION BOX
        // 3. BID DRAFT PANE
        ftCentralPane = new VBox();
//        ftCentralPane.setPrefSize(CENTER_PANE_WIDTH, CENTER_PANE_HEIGHT);

        // NOW THE HEADING
        fantasyTeamsHeadingLabel = initLabel(WDK_PropertyType.FANTASY_TEAM_HEADING_LABEL, CLASS_HEADING_LABEL);
        fantasyTeamsPane.setTop(fantasyTeamsHeadingLabel);

        // NOW THE DRAFT NAME SECTION.
        // NOW THE BID DRAFT SECTION
        draftNameToolbar = new HBox();
        draftNameLabel = initChildLabel(draftNameToolbar, WDK_PropertyType.DRAFT_NAME_LABEL, CLASS_SUBHEADING_LABEL);
        draftNameTextField = initTextField(draftNameToolbar, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true);

        // NOW THE FANTASY TEAM MANIPULATION SECTION
        fantasyTeamToolbar = new HBox();
        addFantasyTeamButton = initChildButton(fantasyTeamToolbar, WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_FANTASY_TEAM_TOOLTIP, false);
        removeFantasyTeamButton = initChildButton(fantasyTeamToolbar, WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_FANTASY_TEAM_TOOLTIP, false);
        editFantasyTeamButton = initChildButton(fantasyTeamToolbar, WDK_PropertyType.EDIT_ICON, WDK_PropertyType.EDIT_FANTASY_TEAM_TOOLTIP, false);

        fantasyTeamSelectionLabel = initChildLabel(fantasyTeamToolbar, WDK_PropertyType.FANTASY_TEAM_SELECTION_LABEL, CLASS_SUBHEADING_LABEL);
        fantasyTeamSelectionBox = initChildComboBox(fantasyTeamToolbar);
        fantasyTeamSelectionBox.setDisable(true);

        // BID DRAFT STARTING LINEUP SECTION
        slPane = new VBox();
        startLineupLabel = initChildLabel(slPane, WDK_PropertyType.STARTING_LINEUP_HEADING_LABEL, CLASS_HEADING_LABEL);
        startLineupTable = new TableView<>();
        startLineupTable.getColumns().addAll(tPositionColumn,
                tFirstColumn,
                tLastColumn,
                tProTeamColumn,
                tPositionsColumn,
                tROrWColumn,
                tHrOrSvColumn,
                tRbiOrKColumn,
                tSbOrEraColumn,
                tBaOrWhipColumn,
                tEstimatedValueColumn,
                tContractColumn,
                tSalaryColumn);
        startLineupTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        slPane.getChildren().add(startLineupTable);
        slPane.getStyleClass().add(CLASS_BORDERED_PANE);

        // TODO, SET SPECIFIC ORDER
        // BID DRAFT TAXI SQUAD SECTION
        tsPane = new VBox();
        taxiSquadLabel = initChildLabel(tsPane, WDK_PropertyType.TAXI_SQUAD_HEADING_LABEL, CLASS_HEADING_LABEL);
        taxiSquadTable = new TableView<>();
        taxiSquadTable.getColumns().addAll(sTaxiColumn, sFirstColumn, sLastColumn,
                sProTeamColumn, sPositionsColumn, sROrWColumn, sHrOrSvColumn, sRbiOrKColumn, sSbOrEraColumn,
                sBaOrWhipColumn, sEstimatedValueColumn, sContractColumn, sSalaryColumn);
        taxiSquadTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tsPane.getChildren().add(taxiSquadTable);
        tsPane.getStyleClass().add(CLASS_BORDERED_PANE);
//        pTaxiColumn, pFirstColumn, pLastColumn, 
//                pProTeamColumn, pROrWColumn, pHrOrSvColumn, pRbiOrKColumn, pSbOrEraColumn, 
//                pBaOrWhipColumn, pEstimatedValueColumn, pContractColumn, pSalaryColumn

        // NOW COLLECT COPONENTS UP
        ftCentralPane.getChildren().add(draftNameToolbar);
        ftCentralPane.getChildren().add(fantasyTeamToolbar);
        ftCentralPane.getChildren().add(slPane);
        ftCentralPane.getChildren().add(tsPane);

        fantasyTeamsPane.setCenter(ftCentralPane);

        // NOW THE SCREEN SELECTION PANE.
        fantasyTeamsPane.setBottom(ScreenToolbarPane);
        // STYLE SHEET
//        fantasyTeamsPane.getStyleClass().add(CLASS_BORDERED_PANE);    
        // SET SCREEN INVISIBLE
        fantasyTeamsPane.setVisible(false);
    }

    // INITIALIZE THE PLAYERS PANE
    private void initPlayersScreen() {
        // NOW THE PLAYERSCREEN PANE
        playerPane = new BorderPane();
        pCentralPane = new VBox();
        pCentralPane.setPrefSize(CENTER_PANE_WIDTH, CENTER_PANE_HEIGHT);

        playerHeadingLabel = initLabel(WDK_PropertyType.AVAILABLE_PLAYERS_HEADING_LABEL, CLASS_HEADING_LABEL);

        // NOW THE ADD / REMOVE BUTTON
        playersToolbar = new HBox();
        addPlayerButton = initChildButton(playersToolbar, WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_PLAYER_TOOLTIP, false);
        deletePlayerButton = initChildButton(playersToolbar, WDK_PropertyType.MINUS_ICON, WDK_PropertyType.DELETE_PLAYER_TOOLTIP, false);

        // NOW THE SEARCH BOX
        searchPlayerLabel = initChildLabel(playersToolbar, WDK_PropertyType.SEARCH_PLAYER_LABEL, CLASS_SUBHEADING_LABEL);
        searchPlayerTextField = initTextField(playersToolbar, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true);

        pCentralPane.getChildren().add(playersToolbar);

        // NOW THE POSITIONS FILTER BAR
        positionSelectionBar = new HBox();
        radioGroup = new ToggleGroup();
        allButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_ALL, radioGroup);
        allButton.setSelected(true);
        cButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_C, radioGroup);
        onebButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_1B, radioGroup);
        ciButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_CI, radioGroup);
        threebButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_3B, radioGroup);
        twobButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_2B, radioGroup);
        miButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_MI, radioGroup);
        ssButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_SS, radioGroup);
        ofButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_OF, radioGroup);
        uButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_U, radioGroup);
        pButton = initChildRadioButton(positionSelectionBar, RADIOBUTTON_LABEL_P, radioGroup);

        positionSelectionBar.setPrefSize(CENTER_PANE_WIDTH, POSITION_SELECTION_PANE_HEIGHT);
        positionSelectionBar.getStyleClass().add(CLASS_RADIO_PANE);
        pCentralPane.getChildren().add(positionSelectionBar);

        // NOW THE PLAYER TABLE
        playerTable = new TableView();
        playerTable.getColumns().addAll(pFirstColumn, pLastColumn, pProTeamColumn, pPositionsColumn, pYearOfBirthColumn,
                pROrWColumn, pHrOrSvColumn, pRbiOrKColumn, pSbOrEraColumn, pBaOrWhipColumn, pEstimatedValueColumn, pNotesColumn);
        pCentralPane.getChildren().add(playerTable);

        // NOW THE STYLE SHEET       
        playerPane.setTop(playerHeadingLabel);
        playerPane.setCenter(pCentralPane);
        playerPane.setBottom(ScreenToolbarPane);

        // SET SCREEN INVISIBLE
        playerPane.setVisible(false);
    }

    // INITIALIZE THE FANTASY STANDINGS PANE
    private void initFantasyStadingsScreen() {
        fantasyStandingsPane = new BorderPane();
        fsCentralPane = new VBox();
        fsCentralPane.setPrefSize(CENTER_PANE_WIDTH, CENTER_PANE_HEIGHT);

        // NOW THE HEADING
        fantasyStandingsHeadingLabel = initLabel(WDK_PropertyType.FANTASY_STADINGS_HEADING_LABEL, CLASS_HEADING_LABEL);
        fantasyStandingsPane.setTop(fantasyStandingsHeadingLabel);

        fantasyStandingTable = new TableView<>();

        fantasyStandingTable.getColumns().addAll(fTeamNameColumn,
                fPlayerNeededColumn,
                fMoneyLeftColumn,
                fMoneyPerPersonColumn,
                fRTotalColumn,
                fHRTotalColumn,
                fRBITotalColumn,
                fSBTotalColumn,
                fBATotalColumn,
                fWTotalColumn,
                fSVTotalColumn,
                fKTotalColumn,
                fERATotalColumn,
                fWHIPTotalColumn,
                fTotalPointsColumn);

        fsCentralPane.getChildren().add(fantasyStandingTable);
        fantasyStandingsPane.setCenter(fsCentralPane);
        fantasyStandingsPane.setBottom(ScreenToolbarPane);
        // STYLE SHEET

        // SET SCREEN INVISIBLE
        fantasyStandingsPane.setVisible(false);
    }

    // INITIALIZE THE DRAFT PANE
    private void initDraftScreen() {
        draftPane = new BorderPane();
        dCentralPane = new VBox();
        dCentralPane.setPrefSize(CENTER_PANE_WIDTH, CENTER_PANE_HEIGHT);

        // NOW THE HEADING
        draftHeadingLabel = initLabel(WDK_PropertyType.DRAFT_HEADING_LABEL, CLASS_HEADING_LABEL);
        draftPane.setTop(draftHeadingLabel);
        
        
        draftControlPane = new FlowPane();
        
        // DRAFT MANUALLY BUTTON
        // AUTO DRAFT
        // PAUSE
        selectPlayerButton = initChildButton(draftControlPane , WDK_PropertyType.SELECT_PLAYER_ICON, WDK_PropertyType.SELECT_PLAYER_TOOLTIP, false);
        startAutoDraft = initChildButton(draftControlPane, WDK_PropertyType.START_ICON, WDK_PropertyType.START_ICON_TOOLTIP, false);
        pauseAutoDraft = initChildButton(draftControlPane, WDK_PropertyType.PAUSE_ICON, WDK_PropertyType.PAUSE_ICON_TOOLTIP, false);
       
        
        // DRAFT SUMMARY TABLE
        draftSummaryTable = new TableView<>();
        draftSummaryTable.getColumns().addAll(dPickNumberColumn, dFirstNameColumn, dLastNameColumn, dTeamNameColumn, dContractColumn, dSalaryColumn);
        dCentralPane.getChildren().addAll(draftControlPane, draftSummaryTable);
        
        draftPane.setCenter(dCentralPane);
        draftPane.setBottom(ScreenToolbarPane);
        // STYLE SHEET
        // SET SCREEN INVISIBLE
        draftPane.setVisible(false);
    }

    // INITIALIZE THE FANTASY TEAM PANE
    private void initMLBTeamsScreen() {
        mlbTeamsPane = new BorderPane();
        mlbtCentralPane = new VBox();
        mlbtCentralPane.setPrefSize(CENTER_PANE_WIDTH, CENTER_PANE_HEIGHT);

        // NOW THE HEADING
        mlbTeamsHeadingLabel = initLabel(WDK_PropertyType.MLB_TEAMS_HEADING_LABEL, CLASS_HEADING_LABEL);

        // NOW THE COMBOBOX
        selectProTeamFlowPane = new FlowPane();
        selectProTeamLabel = initLabel(WDK_PropertyType.SELECT_PRO_TEAM_LABEL, CLASS_SUBHEADING_LABEL);
        selectProTeamFlowPane.getChildren().add(selectProTeamLabel);
        selectProTeamBox = initChildComboBox(selectProTeamFlowPane);

        // NOW THE TABLE
        mlbTeamsTable.getColumns().addAll(
                mFirstColumn,
                mLastColumn,
                mPositionsColumn);
        mlbtCentralPane.getChildren().addAll(selectProTeamFlowPane, mlbTeamsTable);

        mlbTeamsPane.setTop(mlbTeamsHeadingLabel);
        mlbTeamsPane.setCenter(mlbtCentralPane);
        mlbTeamsPane.setBottom(ScreenToolbarPane);
        // STYLE SHEET
        // SET SCREEN INVISIBLE
        mlbTeamsPane.setVisible(false);
    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Draft IS CREATED OR LOADED
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A DRAFT
        wdkPane = new BorderPane();
        wdkPane.setTop(fileToolbarPane);
        primaryScene = new Scene(wdkPane);

        // SET SCREEN INVISIBLE
        centerWorkspacePane.setVisible(true);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    // INIT ALL THE EVENT HANDLERS
    private void initEventHandlers() throws IOException {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(messageDialog, yesNoCancelDialog, draftFileManager, siteExporter);
        newDraftButton.setOnAction(e -> {
            fileController.handleNewDraftRequest(this);
        });
        loadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });
        saveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        exportSiteButton.setOnAction(e -> {
            fileController.handleExportDraftRequest(this);
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });

        // THEN THE SCREEN CONTROLS
        fantasyTeamsButton.setOnAction(e -> {
            changeScreen(FANTASY_TEAMS);
        });
        playersButton.setOnAction(e -> {
            changeScreen(PLAYERS);
        });
        fantasyStandingsButton.setOnAction(e -> {
            changeScreen(FANTASY_STANDINGS);
        });
        draftButton.setOnAction(e -> {
            changeScreen(DRAFT);
        });
        mlbTeamsButton.setOnAction(e -> {
            changeScreen(MLB_TEAM);
        });

        // THEN THE DRAFT EDITING CONTROLS
        draftController = new DraftEditController();

        // NOW THE FANTASY TEAMS SCREEN CONTROLS HANDLES
        FantasyTeamEditController teamController = new FantasyTeamEditController(primaryStage, dataManager.getDraft(), messageDialog, yesNoCancelDialog);
        addFantasyTeamButton.setOnAction(e -> {
            teamController.handleAddFantasyTeamRequest(this);
            draftController.handleDraftChangeRequest(this);
        });
        removeFantasyTeamButton.setOnAction(e -> {
            teamController.handleRemoveFantasyTeamRequest(this, fantasyTeamSelectionBox.getSelectionModel().getSelectedItem());
            draftController.handleDraftChangeRequest(this);
        });
        editFantasyTeamButton.setOnAction(e -> {
            if (!dataManager.getDraft().getFreeAgent().getName().equals(fantasyTeamSelectionBox.getSelectionModel().getSelectedItem())) {
                teamController.handleEditFantasyTeamRequest(this, fantasyTeamSelectionBox.getSelectionModel().getSelectedItem());
                draftController.handleDraftChangeRequest(this);
            }

        });

        // SELECT A TEAM, RELOAD TABLE
        fantasyTeamSelectionBox.setOnAction(e -> {
            draftController.handleSwitchSelectedFantasyTeamRequest(this, fantasyTeamSelectionBox.getSelectionModel().getSelectedItem());
        });

        // THE STARTING LINEUP TABLE
        // WHEN I USE THIS METHOD, I'VE FOUND OUT THAT WHEN I RELOAD THE DATA OF TABLE
        // THEN REFRESH, THE DOUBLE CLICK FUNCTION DOSEN'T WORK
//        startLineupTable.setOnMouseClicked(e -> {
//            if (e.getClickCount() == 2) {
//                // OPEN UP THE PLAYER EDITOR
//                if (startLineupTable.getSelectionModel().getSelectedItem() != null) {
//                    Player p = startLineupTable.getSelectionModel().getSelectedItem();
//                    teamController.handleEditPlayerRequest(this, p, FANTASY_TEAMS);
//                }
//            }
//        });
        startLineupTable.setRowFactory(tv -> {
            TableRow<Player> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Player rowData = row.getItem();
                    teamController.handleEditPlayerRequest(this, rowData, FANTASY_TEAMS);
                    draftController.handleDraftChangeRequest(this);
                }
            });
            return row;
        });

        taxiSquadTable.setRowFactory(tv -> {
            TableRow<Player> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Player rowData = row.getItem();
                    teamController.handleEditPlayerRequest(this, rowData, FANTASY_TEAMS);
                    draftController.handleDraftChangeRequest(this);
                }
            });
            return row;
        });

        /**
         * ***************************************************
         */
        /**
         * Player Screen *
         */
        /**
         * ***************************************************************************
         */
        // PLAYER BUTTONS
        addPlayerButton.setOnAction(e -> {
            teamController.handleAddNewPlayerRequest(this);
            draftController.handleDraftChangeRequest(this);
        });
        deletePlayerButton.setOnAction(e -> {
            teamController.handleDeletePlayerRequest(this, playerTable.getSelectionModel().getSelectedItem());
            draftController.handleDraftChangeRequest(this);
        });
        // THE PLAYERS TABLE
        playerTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                // OPEN UP THE PLAYER EDITOR
                if (playerTable.getSelectionModel().getSelectedItem() != null) {
                    Player p = playerTable.getSelectionModel().getSelectedItem();
                    teamController.handleEditPlayerRequest(this, p, PLAYERS);
                    draftController.handleDraftChangeRequest(this);
                }

            }
        });

        draftNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            dataManager.getDraft().setDraftName(newValue);
            draftController.handleDraftChangeRequest(this);
        });
        
        /**
         * *******************Draft**************************
         */
        
         selectPlayerButton.setOnAction(e->{
                draftController.handleSelectPlayerRequest(this);
         });
         startAutoDraft.setOnAction(e->{
                draftController.handleAutoSelectPlayerRequest(this);
         });
        pauseAutoDraft.setOnAction(e->{});
        
        
        /**
         * *******************Draft**************************
         */
        
        

        /**
         * *******************MLB Team**************************
         */
        selectProTeamBox.setOnAction(e -> {
            String selectTeam = selectProTeamBox.getSelectionModel().getSelectedItem().toString();
            dataManager.getMLBTeamByName(selectTeam);
        });

//        
//        // TEXT FIELDS HAVE A DIFFERENT WAY OF LISTENING FOR TEXT CHANGES
//        registerTextFieldController(draftNumberTextField);
//        registerTextFieldController(draftTitleTextField);
//        registerTextFieldController(instructorNameTextField);
//        registerTextFieldController(instructorURLTextField);
//        // AND NOW THE SCHEDULE ITEM ADDING AND EDITING CONTROLS
//        scheduleController = new ScheduleEditController(primaryStage, dataManager.getDraft(), messageDialog, yesNoCancelDialog);
//        addScheduleItemButton.setOnAction(e -> {
//            scheduleController.handleAddScheduleItemRequest(this);
//        });
//        removeScheduleItemButton.setOnAction(e -> {
//            scheduleController.handleRemoveScheduleItemRequest(this, scheduleItemsTable.getSelectionModel().getSelectedItem());
//        });
//        
//        // AND NOW THE SCHEDULE ITEMS TABLE
//        scheduleItemsTable.setOnMouseClicked(e -> {
//            if (e.getClickCount() == 2) {
//                // OPEN UP THE SCHEDULE ITEM EDITOR
//                ScheduleItem si = scheduleItemsTable.getSelectionModel().getSelectedItem();
//                scheduleController.handleEditScheduleItemRequest(this, si);
//            }
//        });
//        
//        removeAssignmentsButton.setOnAction(e ->{
//            scheduleController.handleRemoveAssignmentRequest(this, assignmentsTable.getSelectionModel().getSelectedItem());
//        });
//        
//        assignmentsTable.setOnMouseClicked(e -> {
//            if (e.getClickCount() == 2){
//                Assignment as = assignmentsTable.getSelectionModel().getSelectedItem();
//                scheduleController.handleEditAssignmentRequest(this, as);
//            }
//        });
    }

    // REGISTER THE EVENT LISTENER FOR A TEXT FIELD
    private void registerTextFieldController(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            draftController.handleDraftChangeRequest(this);
        });
    }

    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initChildLabel(Pane container, WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        container.getChildren().add(label);
        return label;
    }

    // INIT A LABEL AND PLACE IT IN A GridPane INIT ITS PROPER PLACE
    private Label initGridLabel(GridPane container, WDK_PropertyType labelProperty, String styleClass, int col, int row, int colSpan, int rowSpan) {
        Label label = initLabel(labelProperty, styleClass);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }

    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private RadioButton initChildRadioButton(Pane container, String buttonLabel, ToggleGroup group) {
        RadioButton rb = new RadioButton(buttonLabel);
        container.getChildren().add(rb);
        rb.setToggleGroup(group);
        return rb;
    }

// INIT A COMBO BOX AND PUT IT IN A GridPane
    private ComboBox initChildComboBox(Pane container) {
        ComboBox comboBox = new ComboBox();
        container.getChildren().add(comboBox);
        return comboBox;
    }

    // LOAD THE COMBO BOX TO HOLD DRAFT
    private void loadFantasyTeamSelctionComboBox(ObservableList<Team> fantasyTeams) {
        ArrayList<String> selection = new ArrayList<>();
        for (Team team : fantasyTeams) {
            selection.add(team.getName());
        }

        fantasyTeamSelectionBox.setItems(FXCollections.observableArrayList(selection));
    }

    // INIT A TEXT FIELD AND PUT IT IN A GridPane
    private TextField initGridTextField(GridPane container, int size, String initText, boolean editable, int col, int row, int colSpan, int rowSpan) {
        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setText(initText);
        tf.setEditable(editable);
        container.add(tf, col, row, colSpan, rowSpan);
        return tf;
    }

    // INIT A TEXT FIELD
    private TextField initTextField(Pane container, int size, String initText, boolean editable) {
        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setText(initText);
        tf.setEditable(editable);
        container.getChildren().add(tf);
        return tf;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     *
     * Initializes the table columns and sets up sorting and filtering.
     */
    private void initFilteredData() {
        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<Player> filteredData = new FilteredList<>(dataManager.getDraft().getFreeAgent().getPlayersData(), p -> true);

        Predicate<Player> radioPredicate = (player -> {
            System.out.println("Radio Predicate defined");
            if (radioGroup.getSelectedToggle() != null) {
                String position = ((RadioButton) radioGroup.getSelectedToggle()).getText();
                System.out.println("Position:" + position);

                if (((Player) player).getPositions().contains(position)) {
                    return true; // Filter matches selected position.
                }
                if (position.equals(RADIOBUTTON_LABEL_MI)) {
                    if (((Player) player).getPositions().contains(RADIOBUTTON_LABEL_2B)
                            || ((Player) player).getPositions().contains(RADIOBUTTON_LABEL_SS)) {
                        return true;
                    }
                }
                if (position.equals(RADIOBUTTON_LABEL_CI)) {
                    if (((Player) player).getPositions().contains(RADIOBUTTON_LABEL_1B)
                            || ((Player) player).getPositions().contains(RADIOBUTTON_LABEL_3B)) {
                        return true;
                    }
                }
                if (position.equals(RADIOBUTTON_LABEL_U)) {
                    if (!((Player) player).getPositions().contains(RADIOBUTTON_LABEL_P)) {
                        return true;
                    }
                }
                if (position.equals(RADIOBUTTON_LABEL_ALL)) {
                    return true;
                }
            }
            return false;
        });
        Predicate<Player> textPredicate = (player -> {
            System.out.println("text: " + searchPlayerTextField.getCharacters().toString());
            // If filter text is empty, display all persons.
            if (searchPlayerTextField.getCharacters().toString() == null || searchPlayerTextField.getCharacters().toString().isEmpty()) {
                return true;
            }

            // Compare first name and last name of every person with filter text.
            String lowerCaseFilter = searchPlayerTextField.getCharacters().toString().toLowerCase();

            if (((Player) player).getFirstName().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches first name.
            } else if (((Player) player).getLastName().toLowerCase().contains(lowerCaseFilter)) {
                return true; // Filter matches last name.
            }
            return false; // Does not match.
        });

        // 2. Set the filter Predicate whenever the filter changes.
        searchPlayerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(textPredicate.and(radioPredicate));
        });
        radioGroup.selectedToggleProperty().addListener((observableValue, oldToggle, newToggle) -> {
            filteredData.setPredicate(textPredicate.and(radioPredicate));
        });

        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<Player> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(playerTable.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        playerTable.setItems(sortedData);
    }

    // DISABLE COLUMN SORTING
    private void reloadFantasyTeamScreenControls() {
        loadFantasyTeamSelctionComboBox(dataManager.getDraft().getAllTeams());
        tPositionColumn.setSortable(false);
        tFirstColumn.setSortable(false);
        tLastColumn.setSortable(false);
        tProTeamColumn.setSortable(false);
        tPositionsColumn.setSortable(false);
        tROrWColumn.setSortable(false);
        tHrOrSvColumn.setSortable(false);
        tRbiOrKColumn.setSortable(false);
        tSbOrEraColumn.setSortable(false);
        tBaOrWhipColumn.setSortable(false);
        tEstimatedValueColumn.setSortable(false);
        tContractColumn.setSortable(false);
        tSalaryColumn.setSortable(false);
    }

    private void reloadPlayerScreenControls() {
        pFirstColumn.setSortable(true);
        pLastColumn.setSortable(true);
        pProTeamColumn.setSortable(true);
        pPositionsColumn.setSortable(true);
        pROrWColumn.setSortable(true);
        pHrOrSvColumn.setSortable(true);
        pRbiOrKColumn.setSortable(true);
        pSbOrEraColumn.setSortable(true);
        pBaOrWhipColumn.setSortable(true);
        pEstimatedValueColumn.setSortable(true);
        pContractColumn.setSortable(true);
        pSalaryColumn.setSortable(true);
    }

    private void initFantasyTeamsTable() {
        tFirstColumn = new TableColumn(COL_FIRST);
        tLastColumn = new TableColumn(COL_LAST);
        tProTeamColumn = new TableColumn(COL_PRO_TEAM);
        tPositionColumn = new TableColumn(COL_POSITION);
        tPositionsColumn = new TableColumn(COL_POSITIONS);
        tYearOfBirthColumn = new TableColumn(COL_YEAR_OR_BIRTH);
        tROrWColumn = new TableColumn(COL_R_OR_W);
        tHrOrSvColumn = new TableColumn(COL_HR_OR_SV);
        tRbiOrKColumn = new TableColumn(COL_RBI_OR_K);
        tSbOrEraColumn = new TableColumn(COL_SB_OR_ERA);
        tBaOrWhipColumn = new TableColumn(COL_BA_OR_WHIP);
        tEstimatedValueColumn = new TableColumn(COL_EST_VALUE);
        tNotesColumn = new TableColumn(COL_NOTES);
        tContractColumn = new TableColumn(COL_CONTRACT);
        tSalaryColumn = new TableColumn(COL_SALARY);

        tPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        tFirstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        tLastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        tProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("previousTeam"));
        tPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        tYearOfBirthColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("yearOfBirth"));
        tROrWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("rOrW"));
        tHrOrSvColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("hrOrSv"));
        tRbiOrKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("rbiOrK"));
        tSbOrEraColumn.setCellValueFactory(new PropertyValueFactory<String, String>("sbOrEra"));
        tBaOrWhipColumn.setCellValueFactory(new PropertyValueFactory<String, String>("baOrWhip"));
        tEstimatedValueColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("estimatedValue"));
        tContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        tSalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));

    }

    private void initTaxiSquadTable() {
        sTaxiColumn = new TableColumn(COL_TAXI);
        sFirstColumn = new TableColumn(COL_FIRST);
        sLastColumn = new TableColumn(COL_LAST);
        sProTeamColumn = new TableColumn(COL_PRO_TEAM);
        sPositionsColumn = new TableColumn(COL_POSITIONS);
        sYearOfBirthColumn = new TableColumn(COL_YEAR_OR_BIRTH);
        sROrWColumn = new TableColumn(COL_R_OR_W);
        sHrOrSvColumn = new TableColumn(COL_HR_OR_SV);
        sRbiOrKColumn = new TableColumn(COL_RBI_OR_K);
        sSbOrEraColumn = new TableColumn(COL_SB_OR_ERA);
        sBaOrWhipColumn = new TableColumn(COL_BA_OR_WHIP);
        sEstimatedValueColumn = new TableColumn(COL_EST_VALUE);
        sContractColumn = new TableColumn(COL_CONTRACT);
        sSalaryColumn = new TableColumn(COL_SALARY);

        sTaxiColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        sFirstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        sLastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        sProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("previousTeam"));
        sPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        sYearOfBirthColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("yearOfBirth"));
        sROrWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("rOrW"));
        sHrOrSvColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("hrOrSv"));
        sRbiOrKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("rbiOrK"));
        sSbOrEraColumn.setCellValueFactory(new PropertyValueFactory<String, String>("sbOrEra"));
        sBaOrWhipColumn.setCellValueFactory(new PropertyValueFactory<String, String>("baOrWhip"));
        sEstimatedValueColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("estimatedValue"));
        sContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        sSalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));

    }

    private void initAvailablePlayersTable() {
        pFirstColumn = new TableColumn(COL_FIRST);
        pLastColumn = new TableColumn(COL_LAST);
        pProTeamColumn = new TableColumn(COL_PRO_TEAM);
        pPositionsColumn = new TableColumn(COL_POSITIONS);
        pYearOfBirthColumn = new TableColumn(COL_YEAR_OR_BIRTH);
        pROrWColumn = new TableColumn(COL_R_OR_W);
        pHrOrSvColumn = new TableColumn(COL_HR_OR_SV);
        pRbiOrKColumn = new TableColumn(COL_RBI_OR_K);
        pSbOrEraColumn = new TableColumn(COL_SB_OR_ERA);
        pBaOrWhipColumn = new TableColumn(COL_BA_OR_WHIP);
        pEstimatedValueColumn = new TableColumn(COL_EST_VALUE);
        pNotesColumn = new TableColumn(COL_NOTES);
        pContractColumn = new TableColumn(COL_CONTRACT);
        pSalaryColumn = new TableColumn(COL_SALARY);

        pSbOrEraColumn.setComparator(new Comparator<String>() {

            @Override
            public int compare(String s1, String s2) {
                if (s1.equals(s2)) {
                    return 0;
                }
                if (s1.equals(DASH)) {
                    return -1;
                }
                if (s2.equals(DASH)) {
                    return 1;
                }
                int dotIndx1 = s1.indexOf(".");
                int dotIndx2 = s2.indexOf(".");
                String sndLv1;
                String sndLv2;
                int firstLevel1;
                int firstLevel2;
                if (dotIndx1 == -1) {
                    sndLv1 = "0";
                    firstLevel1 = Integer.parseInt(s1);
                } else {
                    firstLevel1 = Integer.parseInt(s1.substring(0, dotIndx1));
                    sndLv1 = s1.substring(dotIndx1 + 1);
                }
                if (dotIndx2 == -1) {
                    sndLv2 = "0";
                    firstLevel2 = Integer.parseInt(s2);
                } else {
                    firstLevel2 = Integer.parseInt(s2.substring(0, dotIndx2));
                    sndLv2 = s2.substring(dotIndx2 + 1);
                }
                if (firstLevel1 > firstLevel2) {
                    return 1;
                } else if (firstLevel1 < firstLevel2) {
                    return -1;
                } else {
                    return compare(sndLv1, sndLv2);
                }
            }
        });
        pBaOrWhipColumn.setComparator(new Comparator<String>() {

            @Override
            public int compare(String s1, String s2) {
                if (s1.equals(s2)) {
                    return 0;
                }
                if (s1.equals(DASH)) {
                    return -1;
                }
                if (s2.equals(DASH)) {
                    return 1;
                }
                int dotIndx1 = s1.indexOf(".");
                int dotIndx2 = s2.indexOf(".");
                String sndLv1;
                String sndLv2;
                int firstLevel1;
                int firstLevel2;
                if (dotIndx1 == -1) {
                    sndLv1 = "0";
                    firstLevel1 = Integer.parseInt(s1);
                } else {
                    firstLevel1 = Integer.parseInt(s1.substring(0, dotIndx1));
                    sndLv1 = s1.substring(dotIndx1 + 1);
                }
                if (dotIndx2 == -1) {
                    sndLv2 = "0";
                    firstLevel2 = Integer.parseInt(s2);
                } else {
                    firstLevel2 = Integer.parseInt(s2.substring(0, dotIndx2));
                    sndLv2 = s2.substring(dotIndx2 + 1);
                }
                if (firstLevel1 > firstLevel2) {
                    return 1;
                } else if (firstLevel1 < firstLevel2) {
                    return -1;
                } else {
                    return compare(sndLv1, sndLv2);
                }
            }
        });
        pFirstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        pLastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        pProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("previousTeam"));
        pPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        pYearOfBirthColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("yearOfBirth"));
        pROrWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("rOrW"));
        pHrOrSvColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("hrOrSv"));
        pRbiOrKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("rbiOrK"));
        pSbOrEraColumn.setCellValueFactory(new PropertyValueFactory<String, String>("sbOrEra"));
        pBaOrWhipColumn.setCellValueFactory(new PropertyValueFactory<String, String>("baOrWhip"));
        pEstimatedValueColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("estimatedValue"));
        pNotesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
        pContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        pSalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));

    }

    private void initFantasyStandingsTable() {
        fTeamNameColumn = new TableColumn(COL_TEAM_NAME);
        fPlayerNeededColumn = new TableColumn(COL_PLAYERS_NEEDED);
        fMoneyLeftColumn = new TableColumn(COL_MONEY_LEFT);
        fMoneyPerPersonColumn = new TableColumn(COL_MONEY_PER_PERSON);
        fRTotalColumn = new TableColumn(COL_R);
        fHRTotalColumn = new TableColumn(COL_HR);
        fRBITotalColumn = new TableColumn(COL_RBI);
        fSBTotalColumn = new TableColumn(COL_SB);
        fBATotalColumn = new TableColumn(COL_BA);
        fWTotalColumn = new TableColumn(COL_W);
        fSVTotalColumn = new TableColumn(COL_SV);
        fKTotalColumn = new TableColumn(COL_K);
        fERATotalColumn = new TableColumn(COL_ERA);
        fWHIPTotalColumn = new TableColumn(COL_WHIP);
        fTotalPointsColumn = new TableColumn(COL_TOTAL_POINTS);

        fTeamNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Team, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Team, String> cellData) {
                return new SimpleStringProperty(cellData.getValue().ownerProperty().get() + "'s " + cellData.getValue().nameProperty().get());
            }
        });
        fPlayerNeededColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("playerNeeded"));
        fMoneyLeftColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("purse"));
        fMoneyPerPersonColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("pp"));
        fRTotalColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("runs"));
        fHRTotalColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("homeRuns"));
        fRBITotalColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("runsBattleIn"));
        fSBTotalColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("stolenBases"));
        fBATotalColumn.setCellValueFactory(new PropertyValueFactory<String, String>("battingAverage"));
        fWTotalColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("wins"));
        fSVTotalColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("saves"));
        fKTotalColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("strikeOuts"));
        fERATotalColumn.setCellValueFactory(new PropertyValueFactory<String, String>("earnRunAverage"));
        fWHIPTotalColumn.setCellValueFactory(new PropertyValueFactory<String, String>("whip"));
        fTotalPointsColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalPoints"));

    }

    private void initDraftSummaryTable() {
        dPickNumberColumn = new TableColumn(COL_PICK_NUMBER);
        dFirstNameColumn = new TableColumn(COL_FIRST);
        dLastNameColumn = new TableColumn(COL_LAST);
        dTeamNameColumn = new TableColumn(COL_TEAM);
        dContractColumn = new TableColumn(COL_CONTRACT);
        dSalaryColumn = new TableColumn(COL_SALARY_DOLLAR);

        dPickNumberColumn.setCellValueFactory(new Callback<CellDataFeatures<Player, Player>, ObservableValue<Player>>() {
            @Override
            public ObservableValue<Player> call(CellDataFeatures<Player, Player> p) {
                return new ReadOnlyObjectWrapper(p.getValue());
            }
        });

        dPickNumberColumn.setCellFactory(new Callback<TableColumn<Player, Player>, TableCell<Player, Player>>() {
            @Override
            public TableCell<Player, Player> call(TableColumn<Player, Player> param) {
                return new TableCell<Player, Player>() {
                    @Override
                    protected void updateItem(Player item, boolean empty) {
                        super.updateItem(item, empty);

                        if (this.getTableRow() != null && item != null) {
                            setText(this.getTableRow().getIndex() + "");
                        } else {
                            setText("");
                        }
                    }
                };
            }
        });
        dPickNumberColumn.setSortable(false);
        dFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        dFirstNameColumn.setSortable(false);
        dLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        dLastNameColumn.setSortable(false);
        dTeamNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Player, String> cellData) {
                Team t = dataManager.getDraft().getTeam(cellData.getValue().getCurrentTeam());
                return new SimpleStringProperty(t.getOwner() + "'s " + t.getName());
            }
        });
        dTeamNameColumn.setSortable(false);
        dContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        dContractColumn.setSortable(false);
        dSalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));
        dSalaryColumn.setSortable(false);

    }

    private void initMLBTeamsTable() {
        // INIT COLUMNS
        mFirstColumn = new TableColumn(COL_FIRST);
        mLastColumn = new TableColumn(COL_LAST);
        mPositionsColumn = new TableColumn(COL_POSITIONS);

        // SET CELL FACTORY
        mFirstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        mLastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        mPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));

        // SET DATA RELATE TO TABLE
        mlbTeamsTable = new TableView();
    }
}
